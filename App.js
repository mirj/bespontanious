import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font, Icon, Permissions, Notifications } from 'expo';
import AppNavigator from './src/navigation/AppNavigator';
import LoadingView from './src/components/LoadingView/LoadingView';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './src/reducers';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { withNetworkConnectivity } from 'react-native-offline';
import FlashMessage from "react-native-flash-message";

//PresistConfig to store State in LocalStore to work Offline
const persistConfig = {
  key: 'root',
  storage: storage,
  stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
  blacklist: ['registration']
};

const pReducer = persistReducer(persistConfig, reducers);
export const store = createStore(pReducer);
const persistor = persistStore(store);

let Main = () => (
  <AppNavigator />
)

Main = withNetworkConnectivity({
  withRedux: true,
  pingServerUrl: string = 'https://www.google.ch/',
})(Main);


export default class App extends React.Component {
  state = {
    isLoadingComplete: false
  };

  componentDidMount() {
    //Unresolved Issue with React Native https://github.com/facebook/react-native/issues/12981
    console.ignoredYellowBox = ['Setting a timer'];
  }

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Provider store={store}>
          <PersistGate loading={<LoadingView />} persistor={persistor}>
            <View style={styles.container}>
              {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
              <Main />
              <FlashMessage position="top" />
            </View>
          </PersistGate>
        </Provider>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        'Robot_light': require('./assets/fonts/Roboto-Light.ttf')
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});


