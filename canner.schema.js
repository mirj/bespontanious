/** @jsx builder */
/* eslint-disable react/prop-types */
import builder from 'canner-script';
import Posts from './schema/posts.schema.js';
import Users from './schema/users.schema.js';
import Moments from './schema/moment.schema.js';
import Abos from './schema/abos.schema.js';
import Typs from './schema/typ.schema.js';
import Cantons from './schema/cantons.schema.js';
import TutorialMoment from './schema/tutorial.schema.js';
import {FirebaseClientStorage} from '@canner/storage';
import firebase from 'firebase';
import {FirebaseRtdbClientConnector} from "canner-graphql-interface";
import Config from 'react-native-config'

/* Initialize Firebase */
const firebaseConfig = {
  apiKey: Config.FB_WEB_API_KEY,
  authDomain: "bespontaneous-782bf.firebaseapp.com",
  databaseURL: "https://bespontaneous-782bf.firebaseio.com",
  projectId: "bespontaneous-782bf",
  storageBucket: "bespontaneous-782bf.appspot.com",
  messagingSenderId: "676636678226"
};

/* initialize Firebase DB */
firebase.initializeApp(firebaseConfig);

firebase.auth().signInWithEmailAndPassword("mirjamthomet@gmail.com", "1234567");
const connector = new FirebaseRtdbClientConnector({
  database: firebase.database()
});
const fileStorage = new FirebaseClientStorage({firebase});

const userColumns = [{
  title: 'Name',
  dataIndex: 'name'
}, {
  title: 'Email',
  dataIndex: 'email'
}, {
  title: 'Age',
  dataIndex: 'age'
}];

const postColumns = [{
  title: 'Title',
  dataIndex: 'title'
}];

const abosColumns = [{
  title: 'Title',
  dataIndex: 'title'
}];

const cantonsColumns = [{
  title: 'Title',
  dataIndex: 'title'
}];

const typsColumns = [{
  title: 'Name',
  dataIndex: 'name'
}];

const momentsColumns = [{
  title: 'Title',
  dataIndex: 'title'
}];

const tutorialColumns = [{
  title: 'Title',
  dataIndex: 'title'
}];

export default (
  <root connector={connector} fileStorage={fileStorage} imageStorage={fileStorage} >
    <Posts columns={postColumns} />
    <Users columns={userColumns} />
    <Abos columns={abosColumns} />
    <Typs columns={typsColumns} />
    <Cantons columns={cantonsColumns} />
    <Moments columns={momentsColumns} />
    <TutorialMoment columns={tutorialColumns} />
  </root>
);
