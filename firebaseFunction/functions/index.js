const functions = require('firebase-functions');
const { Expo } = require('expo-server-sdk')
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();
let expo = new Expo();


TEST_RECEIPTS = {
    "960e56de-f6ee-4317-80a9-e7625b1580a1": {
        "status": "error",
        "message": "The Apple Push Notification service failed to send the notification",
        "details": {
            "error": "DeviceNotRegistered"
        },
    },
    "2b1f538b-67d5-421d-8967-34a48387bd24": {
        "status": "ok"
    }
}


/**
 * Generate message objects which will be used to create notifications.
 * A message contains the users expo push token and the message content
 */
function generatePushMessages(pushTokens, moment) {
    console.log("[generatePushMessages]")
    // Create the messages that you want to send to clients
    let messages = [];
    for (let pushToken of pushTokens) {
        // Check that all your push tokens appear to be valid Expo push tokens
        if (!Expo.isExpoPushToken(pushToken)) {
            continue;
        }
        // Construct a message
        messages.push({
            to: pushToken,
            sound: 'default',
            tile: moment.title,
            body: moment.pushMessage
        })
    }

    return messages;
}

/**
 * Create a chunk(batch) of messages and send push notifications in batch requests.
 * Each message contains the ExpoToken of the user and the message title and body.
 * All tickets are stored in the realtime database.
 * @param {*Array} messages All the notifications to be sent
 */
function sendNotifications(messages) {
    console.log("[sendNotifications] Sending Notifications")
    console.log(messages)
    let chunks = expo.chunkPushNotifications(messages);
    let tickets = [];
    // Send the chunks to the Expo push notification service.
    for (let chunk of chunks) {
        expo.sendPushNotificationsAsync(chunk).then((response) => {
            let ticketChunk = response;
            for (let ticket of ticketChunk) {
                tickets.push({ "id": ticket.id, "status": ticket.status, "token": chunk[ticketChunk.indexOf(ticket)].to })
            }
            saveTickets(tickets)
            console.log(tickets)
            return true
        }).catch((error) => {
            console.log("[ERROR: sendNotifications]");
            console.log(error)
            return false
        });
    }
    return true
}

/**
 * Store push notification tickets to firebase. A ticket is generated for every notification.
 * This is used to fetch the receipts which contain errors that need to be handled later.
 * @param {Array} tickets 
 */
function saveTickets(tickets) {
    for (ticket of tickets) {
        admin.database().ref('/tickets/' + ticket.id).set({
            status: ticket.status,
            token: ticket.token
        }, function (error) {
            if (error) {
                console.log("[ERROR saveTickets]" + error)
                console.log(error)
            }
        });
    }
}

/**
 * Handle any errors in the recipts from the notification ticket requests.
 * https://docs.expo.io/versions/latest/guides/push-notifications#receipt-response-format
 * @param {*} receipts 
 * @param {*} test 
 */
function handleReceipts(receipts) {
    for (var receiptID of Object.keys(receipts)) {
        receipt = receipts[receiptID]
        if (receipt.status === 'error') {
            if (receipt.details && receipt.details.error) {
                if (receipt.details.error === "DeviceNotRegistered") {
                    removeUserToken(receiptID)
                }
            }
        } else {
            removeTicket(receiptID)
        }
    }

    return true;
}

/**
 * Get the token stored in the tickets table. passes this to getUserWithToken to find the user and remove the token.
 * @param {} receiptId 
 */
function removeUserToken(receiptId) {
    let tickets;
    admin.database().ref('tickets').once('value', function (snapshot) {
        tickets = snapshot.val()
    }).then(() => {
        console.log(tickets)
        for (let ticketID of Object.keys(tickets)) {
            if (receiptId === ticketID) {
                getUserWithToken(tickets[ticket].token)
            }
        }
        return true
    }).catch((error) => {
        console.log(error)
    })
}

/**
 * Get the user with the given token and pass it to removeToken to delete the token from the user.b
 * @param {*} token 
 */
function getUserWithToken(token) {
    let user;
    admin.database().ref('users').orderByChild("token").equalTo(token).once('value', function (snapshot) {
        user = snapshot.val()
    }).then(() => {
        console.log("Removing token for:")
        console.log(user)
        removeToken(user)
        return true
    }).catch((error) => {
        console.log(error)
    });
}

/**
 * Remove the given user's token
 * @param {} user 
 */
function removeToken(user) {
    admin.database().ref('users/' + user.uid + '/token').remove()
}

/**
 * Remove the given user's token
 * @param {} user 
 */
function removeTicket(ticketId) {
    admin.database().ref('ticket/' + ticketId).remove()
}

/**
 * Adds given moment to given users moments
 * @param {} user 
 * @param {*} moment 
 */
function addMomentToUser(user, moment) {
    let userMoment = {}

    admin.database().ref('users/' + user.uid + '/moments').push({
        abos: moment.momentAbos ? moment.momentAbos : "",
        title: moment.title ? moment.title : "",
        message: moment.pushMessage ? moment.pushMessage : "",
        status: 0,
        steps: moment.steps ? moment.steps : "",
        points: moment.points ? moment.points : "",
        description: moment.description ? moment.description : "",
        seen: false,
        created: Date.now()
    }, function (error) {
        if (error) {
            console.log(error)
        }
    });
}

class Award {
    constructor(title) {
        this.title = title;
    }
}

function checkTutorialCompleted(user) {
    console.log("checkTutorialCompleted")
    let moments = Object.keys(user.moments).map(function (key) {
        return user.moments[key];
    });
    for (moment of moments) {
        if (moment.title === "Tutorial Moment" && moment.status === 3) {
            return true
        }
    }
    return false
}

function calculateAwards(user) {
    console.log("calculateAwards")
    let awards = []
    if (user.momentsComplete > 4) {
        awards.push(new Award('5 moments'))
    }
    if (user.momentsComplete > 9) {
        awards.push(new Award('10 moments'))
    }
    if (user.momentsComplete > 19) {
        awards.push(new Award('20 moments'))
    }
    if (user.momentsComplete > 24) {
        awards.push(new Award('24 moments'))
    }
    if (user.momentsComplete > 99) {
        awards.push(new Award('100 moments'))
    }
    if (user.points > 99) {
        awards.push(new Award('100 Challenge points'))
    }

    let tutorialCompleted = checkTutorialCompleted(user)
    if (tutorialCompleted) {
        awards.push(new Award('Training Wheels'))
    }
    if (user.featuresUsed && user.featuresUsed.generator) {
        awards.push(new Award('Illusion of Choice'))
    }

    admin.database().ref('users/' + user.uid).update({
        awards
    }, function (error) {
        if (error) {
            console.log(error)
        }
    });
}

function getUserAwards(userUid) {
    console.log("getUserAwards")
    let user
    admin.database().ref('users/' + userUid).once('value', function (snapshot) {
        user = snapshot.val()
    }).then(() => {
        calculateAwards(user)
        return true
    }).catch((error) => {
        console.log(error)
    })
}


/**
 * Send push notifications when a new moment is created. 
 * Notifications are sent to users subscribed to the abos in the moment object.
 */
exports.sendPushMessage = functions.database.ref('/moments/{momentId}').onCreate((snapshot, context) => {
    let newMoment = snapshot.val();
    // Fetch base moment objects
    let abos
    admin.database().ref('momentAbos').once('value', function (snapshot) {
        abos = snapshot.val()
    }).then(() => {
        // Get newly created moment

        // Filter the abos and return active abos contained in momentAbos.
        var abosToPush = Object.keys(abos).map(function(aboKey) {
            let abo = abos[aboKey]
            if (abo.active && aboKey in newMoment.momentAbos) {
                return aboKey;
            }
        });
        console.log(abosToPush)
        let typs = Object.keys(newMoment.momentTyps)
        let cantons = Object.keys(newMoment.momentCantons)

        // Get all users and add the moment to their account add their pushTokens to an array.
        // if they match the moment criteria.
        let pushTokens = [];
        let ref = admin.database().ref('users');
        ref.once('value', function (snapshot) {
            let users = snapshot.val();
            for (var userID in users) {
                var user = users[userID];
                var getPush = false;

                if (user.abos && user.username) {
                    // Check if user abos match moment abos
                    user.abos.forEach(abo => {
                        if (abosToPush.includes(abo) && !getPush) {
                            // Check if users location and typ match the moment.
                            if (typs.includes(user.typ.key) && cantons.includes(user.canton.key)) {
                                if (user.token) pushTokens.push(user.token);
                                console.log(user.username)
                                addMomentToUser(user, newMoment)
                                getPush = true;
                            }

                        }
                    });
                }
            }

            let messages = generatePushMessages(pushTokens, newMoment);
            sendNotifications(messages);
        });
        return true
    }).catch((error) => {
        console.log(error)
        return false
    })

    return true;
});

exports.getTicketReceipts = functions.database.ref('/tickets').onUpdate((change) => {
    let receiptIds = [];
    let expo = new Expo();
    admin.database().ref('tickets').once('value', function (snapshot) {
        tickets = snapshot.val()
    }).then(() => {
        receiptIds.push(Object.keys(tickets))
        let receiptIdChunks = expo.chunkPushNotificationReceiptIds(receiptIds);

        for (let chunk of receiptIdChunks) {
            expo.getPushNotificationReceiptsAsync(chunk).then((receipts) => {
                handleReceipts(receipts)
                return true
            }).catch((error) => {
                console.log("getPushNotificationReceiptsAsync:" + error);
                return false
            });
        }

        return true
    }).catch((error) => {
        console.log(error)
    })

    return true
});

exports.calculateAwards = functions.database.ref('/users/{userUid}/moments/{momentUid}/status').onUpdate((change, context) => {
    let userUid = context.params.userUid
    let momentUid = context.params.momentUid
    let statusBefore = change.before.val()
    let statusAfter = change.after.val()
    let momentPath = 'users/' + userUid + '/moments/' + momentUid
    let moment

    // When user finishes a moment
    if (statusAfter === 3) {
        admin.database().ref(momentPath).once('value', function (snapshot) {
            moment = snapshot.val()
        }).then(() => {
            var userPointsPath = admin.database().ref('users/' + userUid + '/points');
            var userMomentsPath = admin.database().ref('users/' + userUid + '/momentsComplete');
            userPointsPath.transaction(function (currentPoints) {
                return (currentPoints || 0) + moment.points;
            })
            userMomentsPath.transaction(function (currentMomentsCompleted) {
                return (currentMomentsCompleted || 0) + 1;
            }).then(() => {
                getUserAwards(userUid)
                return true
            }).catch((error) => {
                console.log(error)
            })
            return true
        }).catch((error) => {
            console.log(error)
        })
    }

    return true
});

exports.calculateFeatureAwards = functions.database.ref('/users/{userUid}/featuresUsed/').onWrite((change, context) => {
    let userUid = context.params.userUid
    getUserAwards(userUid)

    return true
})
