// Chai is a commonly used library for creating unit test suites. It is easily extended with plugins.
const chai = require('chai');
const assert = chai.assert;

// Sinon is a library used for mocking or verifying function calls in JavaScript.
const sinon = require('sinon');

// Require firebase-admin so we can stub out some of its methods.
const admin = require('firebase-admin');

const test = require('firebase-functions-test')({
    apiKey: "AIzaSyB6O1LN3e0wAW5ZcuAIXT_Dh0lk1plAXJo",
    authDomain: "bespontaneous-782bf.firebaseapp.com",
    databaseURL: "https://bespontaneous-782bf.firebaseio.com",
    projectId: "bespontaneous-782bf",
    storageBucket: "bespontaneous-782bf.appspot.com",
    messagingSenderId: "676636678226"
});

describe('Cloud Functions', () => {
    let myFunctions, adminInitStub;

    before(() => {
        myFunctions = require('../index');
    });

    after(() => {
        // Do other cleanup tasks.
        test.cleanup();
    });

    describe('sendPushMessage', () => {
        // Test Case: Sending a moment to sendPushMessage should send push notifications to users subscribed.
        it('should send notfications to users subscribed to 1,2,3,4 or abos', () => {
            const testMoment = {
                val: function () {
                    return {
                        "__typename": "Moment",
                        "momentCantons": {
                            "-LU1m_Set9us8TYeUDNt": true,
                            "-LU1mgqf58ozElUypJrX": true,
                            "-LU1miZ1GwmOIDjsB-Xx": true,
                            "-LU1mkMSX7YhyRKhUvhy": true,
                            "-LU1mmbysGsBS4P7i0la": true,
                            "-LU1n2HUITHKogzCON4v": true,
                            "-LU1n3x4ADnsacoINfAj": true,
                            "-LU1n5dOWlORM4Gb4R-F": true,
                            "-LU1n7EFZgC_KppC1pHu": true,
                            "-LU1nCFvs_sa05L-RsyI": true
                        },
                        "momentTyps": {
                            "-LU1q5mQmBrQM1rcvL-b": true,
                            "-LU1qGSNhfiiaCTo0GcO": true,
                            "-LU1qR8-EohTMqYF0JPp": true,
                            "-LU1q_8e2GoygzPtdMkI": true
                        },
                        "momentAbos" : {
                            "-LTy0aeMESIFD5P-D_tN" : true,
                            "-LTyHGb7JppHnpR7hAwU" : true,
                            "-LTyHZRFZjDu-XNyCHDu" : true,
                            "-LTyHjOuIARJEdfW_fiV" : true
                        },
                        "points": 15,
                        "pushMessage": "Local Firebase Functions Test",
                        "steps": [{
                            "description": "Steige nie mehr aus",
                            "title": "1 Gehe in den Zug"
                        }, {
                            "description": "tschüss",
                            "title": "2 Stirb"
                        }],
                        "title": "Erster Moment"
                    }

                },
            };
            const wrapped = test.wrap(myFunctions.sendPushMessage);

            return assert.equal(wrapped(testMoment), true);
            // [END assertOffline]
        })
    });
    describe('getTicketReceipts', () => {
        // Test Case: Sending a ticket change object should return true if receipts have been retreived and handled.
        it('should send notfications to users subscribed to 1,2,3,4 or abos', () => {
            const ticketChange = {
                "before": {
                    val: function () {
                        return [
                            { "id": "2b1f538b-67d5-421d-8967-34a48234234387bd24" },
                            { "id": "2b1f538b-67d5-421d-8967-34a4342348387bd24" },
                        ]
                    }
                },
                "after": {
                    val: function () {
                        return [
                            { "id": "2ba079bf-06ac-459a-a0ad-dd996b523de5" },
                            { "id": "2ba079bf-06ac-459a-a0ad-dd996b523de7" },
                            { "id": "2ba079bf-06ac-459a-a0ad-dd996b523de9" }
                        ]
                    }
                },
            }

            const wrapped = test.wrap(myFunctions.getTicketReceipts);

            return assert.equal(wrapped(ticketChange), true);
        })
    });
    describe('calculateAwards', () => {
        // Test Case: Sending a ticket change object should return true if receipts have been retreived and handled.
        it('should update the users total points and fetch new awards', () => {
            const statusChange = {
                "before": {
                    val: function () {
                        return 2
                    }
                },
                "after": {
                    val: function () {
                        return 3
                    }
                },
            }

            const wrapped = test.wrap(myFunctions.calculateAwards);
            let context = {
                params: {
                    userUid: '61Buai6TxfV4Qin9WEkOT3IyMUT2',
                    momentUid: '-LV3TIsJkX09wAxbKNW3'
                }
            }
            return assert.equal(wrapped(statusChange, context), true);
        })
    });
})