/** @jsx builder */
/* eslint-disable react/prop-types */
import builder, {Focus, Default} from 'canner-script';

const Abos = ({attributes}) => <array
  keyName="momentAbos"
  ui="tableRoute"
  title="Abos"
  uiParams={{
    columns: attributes.columns
  }}
  imageStorage={attributes.imageStorage}
>
  <toolbar>
    <pagination />
  </toolbar>
  <Focus focus={['title', 'description']}>
    <string keyName="title" title="Titel" required />
    <string keyName="description" title="Beschreibung" required />
    <boolean keyName="active" ui="switch" title="Aktivieren"/>
    <image keyName="aboImage" title="Abo Image"/>
  </Focus>
</array>;

export default Abos;

