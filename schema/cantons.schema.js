/** @jsx builder */
/* eslint-disable react/prop-types */
import builder, { Focus, Default } from 'canner-script';

const Cantons = ({ attributes }) => <array
  keyName="momentCantons"
  ui="tableRoute"
  title="Cantons"
  uiParams={{
    columns: attributes.columns
  }}
>
  <toolbar>
    <pagination />
  </toolbar>
  <Focus focus={['title']}>
    <string keyName="title" title="Titel" required />
  </Focus>
</array>;

export default Cantons;

