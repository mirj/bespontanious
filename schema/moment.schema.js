/** @jsx builder */
/* eslint-disable react/prop-types */
import builder, { Focus, Default } from 'canner-script';

const Moments = ({ attributes }) => <array
  keyName="moments"
  ui="tableRoute"
  title="moments"
  uiParams={{
    columns: attributes.columns
  }}
  imageStorage={attributes.imageStorage}
>
  <toolbar>
    <pagination />
  </toolbar>
  <Focus focus={['title', 'pushMessage', 'description', 'steps', 'momentAbos', 'momentCantons', 'momentTyps']}>
    <string keyName="title" title="Titel" required />
    <string keyName="pushMessage" title="Push-Nachricht" required />
    <string keyName="description" title="Beschreibung Moment" ui="textarea" />
    <number keyName="points" title="Punkte" required />
    <array keyName="steps" title="Schritte" ui="panel" uiParams={{ title: "Schritt" }}>
      <string keyName="title" title="Titel" required />
      <string keyName="description" title="Beschreibung" required />
    </array>
    <relation
      keyName="momentAbos"
      uiParams={{columns: [
        {"title": "Abo Name",
        "dataIndex": "title"}
      ]}}
      ui="multipleSelect"
      relation={{
        to: 'momentAbos', // should be a keyName of first level
        type: 'toMany'
      }}
    />
    <relation
      keyName="momentCantons"
      uiParams={{columns: [
        {"title": "Cantons",
        "dataIndex": "title"}
      ]}}
      ui="multipleSelect"
      relation={{
        to: 'momentCantons', // should be a keyName of first level
        type: 'toMany'
      }}
    />
    <relation
      keyName="momentTyps"
      uiParams={{columns: [
        {"title": "Typs",
        "dataIndex": "name"}
      ]}}
      ui="multipleSelect"
      relation={{
        to: 'momentTyps', // should be a keyName of first level
        type: 'toMany'
      }}
    />
  </Focus>
</array>;

export default Moments;

