/** @jsx builder */
/* eslint-disable react/prop-types */
import builder, { Focus, Default } from 'canner-script';

const Posts = ({ attributes }) => <array
  keyName="posts"
  ui="tableRoute"
  title="posts"
  uiParams={{
    columns: attributes.columns
  }}
  imageStorage={attributes.imageStorage}
>
  <toolbar>
    <pagination />
    <filter>
      <selectFilter
        label="Type"
        options={[{
          text: 'All',
          condition: {
          }
        }, {
          text: 'Draft',
          condition: {
            status: {
              draft: {
                eq: true
              }
            }
          }
        }, {
          text: 'Trashed',
          condition: {
            status: {
              trashed: {
                eq: true
              }
            }
          }
        }]}
      />
    </filter>
  </toolbar>
  <Focus focus={['title', 'preview', 'link']}>
    <string keyName="title" title="Titel" />
    <string keyName="preview" title="Vorschau" ui="textarea" required />
    <string keyName="link" title="Link" />
    <image keyName="featureImage" title="Vorschaubild" />
  </Focus>
</array>;

export default Posts;

