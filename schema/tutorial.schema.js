/** @jsx builder */
/* eslint-disable react/prop-types */
import builder, { Focus, Default } from 'canner-script';

const TutorialMoment = ({ attributes }) => <array
  keyName="tutorialMoment"
  ui="tableRoute"
  title="Tutorial Moment"
  uiParams={{
    columns: attributes.columns
  }}
  imageStorage={attributes.imageStorage}
>
  <toolbar>
    <pagination />
  </toolbar>
  <Focus focus={['title','description', 'steps']}>
    <string keyName="title" title="Titel" required />
    <string keyName="description" title="Beschreibung Moment" ui="textarea" required />
    <number keyName="points" title="Punkte" required />
    <array keyName="steps" title="Schritte" ui="panel" uiParams={{ title: "Schritt" }}>
      <string keyName="title" title="Titel" required />
      <string keyName="description" title="Beschreibung" required />
    </array>
    
  </Focus>
</array>;

export default TutorialMoment;

