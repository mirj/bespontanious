/** @jsx builder */
/* eslint-disable react/prop-types */
import builder, {Focus, Default} from 'canner-script';

const Typs = ({attributes}) => <array
  keyName="momentTyps"
  ui="tableRoute"
  title="Typs"
  uiParams={{
    columns: attributes.columns
  }}
>
  <toolbar>
    <pagination />
  </toolbar>
  <Focus focus={['number', 'description', 'name']}>
    <string keyName="description" title="Beschreibung" required />
    <string keyName="name" title="Name" required />
    <number keyName="number" title="Nummer" required />
    <image keyName="avatarImage" title="Avatar"/>
  </Focus>
</array>;

export default Typs;

