import {
    UPDATE_EMAIL_REGISTRATION,
    UPDATE_PASSWORD_REGISTRATION,
    UPDATE_STEP_REGISTRATION,
    UPDATE_USERNAME_REGISTRATION,
    UPDATE_BIRTHDAY_REGISTRATION,
    UPDATE_TYP_REGISTRATION,
    UPDATE_UID_USER,
    UPDATE_ERROR_REGISTRATION,
    UPDATE_CANTON_REGISTRATION,
    PASSWORD_TOO_SHORT_REGISTRATION,
    INPUT_MISSING_REGISTRATION,
    UPDATE_EMAIL_USER,
    UPDATE_BIRTHDAY_USER,
    UPDATE_TYP_USER,
    UPDATE_ABOS_USER,
    UPDATE_USERNAME_USER,
    UPDATE_CANTON_USER,
    UPDATE_ABOS,
    UPDATE_NEWS,
    UPDATE_USERTOKEN,
    UPDATE_MOMENTS_USER,
    UPDATE_MOMENT_STATE,
    CLEAR_REGISTRATION,
    CLEAR_USER,
    UPDATE_USER_POINTS,
    UPDATE_CONNECTIVITY_TYPE,
    FAKE_CONNECTION_CHANGE,
    UPDATE_USER_NOTIFICATION,
    UPDATE_SEENMODAL_USER,
    UPDATE_ABO_SLIDE,
    UPDATE_USER_TUTORIAL,
    UPDATE_USER_AWARDS,
    UPDATE_USER_FEATURES_USED,
    UPDATE_USER_REGISTRATION_COMPLETE
} from '../actions/types';

/*
USER ACTIONS
 */
export const updateUIDUser = user => ({
    type: UPDATE_UID_USER,
    payload: user
})

export const updateEmailUser = email => ({
    type: UPDATE_EMAIL_USER,
    payload: email
})

export const updateUsernameUser = username => ({
    type: UPDATE_USERNAME_USER,
    payload: username
})

export const updateBirthdayUser = birthday => ({
    type: UPDATE_BIRTHDAY_USER,
    payload: Date(birthday)
})

export const updateTypUser = typ => ({
    type: UPDATE_TYP_USER,
    payload: typ
})

export const updateAbosUser = abos => ({
    type: UPDATE_ABOS_USER,
    payload: abos
})

export const updateUserToken = token => ({
    type: UPDATE_USERTOKEN,
    payload: token
})

export const updateUserPoints = points => ({
    type: UPDATE_USER_POINTS,
    payload: points
})

export const updateUserAwards = awards => ({
    type: UPDATE_USER_AWARDS,
    payload: awards
})

export const updateUserTutorial = hasTutorial => ({
    type: UPDATE_USER_TUTORIAL,
    payload: hasTutorial
})

export const registrationComplete = complete => ({
    type: UPDATE_USER_REGISTRATION_COMPLETE,
    payload: complete
})


export const fakeConnectionChange = isConn => ({
    type: FAKE_CONNECTION_CHANGE,
    payload: isConn
})

export const updateMomentsUser = moments => ({
    type: UPDATE_MOMENTS_USER,
    payload: moments
})

export const updateMomentState = state => ({
    type: UPDATE_MOMENT_STATE,
    payload: state
})

export const clearUser = input => ({
    type: CLEAR_USER,
    payload: input
})

export const updateCantonUser = canton => ({
    type: UPDATE_CANTON_USER,
    payload: canton
})

export const updateConnectivity = connectivity => ({
    type: UPDATE_CONNECTIVITY_TYPE,
    payload: connectivity
})

export const updateNotification = notification => ({
    type: UPDATE_USER_NOTIFICATION,
    payload: notification
})

export const updateUserFeaturesUsed = featuresUsed => ({
    type: UPDATE_USER_FEATURES_USED,
    payload: featuresUsed
})

export const updateSeenModal = seenModal => ({
    type: UPDATE_SEENMODAL_USER,
    payload: seenModal
})

/*
REGISTRATION ACTIONS
 */
export const updateEmailRegistration = email => ({
    type: UPDATE_EMAIL_REGISTRATION,
    payload: email
})

export const updatePasswordRegistration = password => ({
    type: UPDATE_PASSWORD_REGISTRATION,
    payload: password
})

export const setRegistrationStep = registrationStep => ({
    type: UPDATE_STEP_REGISTRATION,
    payload: registrationStep
})

export const updateCantonRegistration = canton => ({
    type: UPDATE_CANTON_REGISTRATION,
    payload: canton
})

export const updateUsernameRegistration = username => ({
    type: UPDATE_USERNAME_REGISTRATION,
    payload: username
})

export const updateBirthdayRegistration = birthday => ({
    type: UPDATE_BIRTHDAY_REGISTRATION,
    payload: Date(birthday)
})

export const updateTypRegistration = typ => ({
    type: UPDATE_TYP_REGISTRATION,
    payload: typ
})

export const updateErrorRegistration = error => ({
    type: UPDATE_ERROR_REGISTRATION,
    payload: error
})

export const inputMissingRegistration = (field, isMissing) => ({
    type: INPUT_MISSING_REGISTRATION,
    payload: isMissing,
    field: field
})

export const passwordTooShortRegistration = input => ({
    type: PASSWORD_TOO_SHORT_REGISTRATION,
    payload: input
})

export const clearRegistration = input => ({
    type: CLEAR_REGISTRATION,
    payload: input
})

/*
ABOS ACTIONS
 */
export const updateAbos = abos => ({
    type: UPDATE_ABOS,
    payload: abos
})

export const updateAboSlide = aboSlide => ({
    type: UPDATE_ABO_SLIDE,
    payload: aboSlide
})

/*
NEWS ACTIONS
 */
export const updateNews = news => ({
    type: UPDATE_NEWS,
    payload: news
})