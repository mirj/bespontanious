export const UPDATE_EMAIL_REGISTRATION = 'update_email_registration';
export const UPDATE_PASSWORD_REGISTRATION = 'update_password_registration';
export const UPDATE_STEP_REGISTRATION = 'update_step_registration';
export const UPDATE_USERNAME_REGISTRATION = 'update_username_registration';
export const UPDATE_BIRTHDAY_REGISTRATION = 'update_birthday_registration';
export const UPDATE_CANTON_REGISTRATION = 'update_canton_registration';
export const UPDATE_TYP_REGISTRATION = 'update_typ_registration';
export const UPDATE_UID_USER = 'UPDATE_UID_USER';
export const UPDATE_ERROR_REGISTRATION = 'error_changed';
export const EMAIL_MISSING_REGISTRATION = 'email_missing_registration';
export const PASSWORD_MISSING_REGISTRATION = 'password_missing_registration';
export const INPUT_MISSING_REGISTRATION = 'input_missing_registration';
export const PASSWORD_TOO_SHORT_REGISTRATION = 'password_too_short_registration';
export const UPDATE_EMAIL_USER = 'update_email_user';
export const UPDATE_USERNAME_USER = 'update_username_user';
export const UPDATE_BIRTHDAY_USER = 'update_birthday_user';
export const UPDATE_TYP_USER = 'update_typ_user';
export const UPDATE_ABOS_USER = 'update_abos_user';
export const UPDATE_ABOS = 'update_abos';
export const UPDATE_NEWS = 'update_news';
export const UPDATE_USERTOKEN = 'update_usertoken';
export const UPDATE_MOMENTS_USER = 'update_moments_user';
export const UPDATE_MOMENT_STATE = 'update_moment_state';
export const UPDATE_USER_NOTIFICATION = 'update_user_notification';
export const CLEAR_REGISTRATION = 'clear_registration';
export const CLEAR_USER = 'clear_user';
export const UPDATE_USER_POINTS = 'update_user_points';
export const UPDATE_CANTON_USER = 'update_canton_user';
export const UPDATE_CONNECTIVITY_TYPE = 'update_connectivity_type';
export const FAKE_CONNECTION_CHANGE = 'fake_connection_change';
export const UPDATE_SEENMODAL_USER = 'update_seen_modal';
export const UPDATE_ABO_SLIDE = 'update_abo_slide';
export const UPDATE_USER_TUTORIAL = 'update_user_tutorial';
export const UPDATE_USER_AWARDS = 'update_user_awards';
export const UPDATE_USER_FEATURES_USED = 'update_user_features_used';
export const UPDATE_USER_REGISTRATION_COMPLETE = 'update_user_registration_complete';
export const MOMENTS_SYNCHED = 'moments_synched';





