import React from 'react';
import { ImageBackground, ScrollView, Dimensions } from 'react-native';
import { Card, CardItem, Button, Text, View } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { updateAbos, updateAbosUser, updateAboSlide } from '../../actions';
import stylesAbo from './stylesAbo';
import stylesButton from '../../constants/Styling/stylesButton';
import stylesCard from '../../constants/Styling/stylesCard';
import stylesFont from '../../constants/Styling/stylesFont';
import SnackBar from 'react-native-snackbar-component';
import { showMessage } from "react-native-flash-message";

/**
 * Component for the abos where user can suscribe/desuscribe
 */
class Abos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            abosLoaded: false,
            showModal: false,
            showDescription: "",
            currentAboTitle: "",
            currentAboUid: "",
            showSnackBar: false,
            snackBarMessage: "",
            aboPopupLifetime: 3000
        }
        this.timerHandle = null
    }

    /**
     * Subscribe/Unsubscribe to abo with given ID. Updates both firebase and redux.
     * @param {*Integer} aboID Id of the abo to subscribe/unsubscribe from
     */
    _onSubscribe(aboTitle, aboUid) {
        if (!this.props.isConnected) {
            showMessage({
                message: "Keine verbindung",
                description: "Diese Funktion ist offline nicht verfügbar",
                type: "default",
                backgroundColor: "#FCEC45", // background color
                color: "#707070"
            });
        } else {
            // Clear notification timer
            if (this.timerHandle) clearTimeout(this.timerHandle)

            let currentAbos = this.props.user.abos
            let added = false

            // Add abo if it doesnt exist, remove if it does.
            if (!this.props.user.abos.includes(aboUid)) {
                added = true
                currentAbos.push(aboUid)
            } else if (this.props.user.abos.length > 0) {
                index = this.props.user.abos.indexOf(aboUid);
                currentAbos.splice(index, 1);
            }

            // Update redux store and firebase DB
            this.props.updateAbosUser(currentAbos);
            let abos = this.props.user.abos
            firebase.database().ref('users/' + this.props.user.uid).update({
                abos
            }, function (error) {
                if (error) {
                    console.log(error)
                }
            });

            // Show the notification SnackBar
            let messageText = added ? "Du erhälst nun Momente von " : "Du erhälst keine Momente mehr von "
            this.setState({ showSnackBar: true, currentAboUid: aboUid, currentAboTitle: aboTitle, snackBarMessage: messageText })

            // Hide snackbar after the popup lifetime (aboPopupLifetime) passes
            this.timerHandle = setTimeout(() => {
                this.setState({
                    showSnackBar: false
                })
            }, this.state.aboPopupLifetime);
        }
    }

    /**
     * Slides in the description of an abo
     * @param {*Integer} key of the abo which was clicked
     */
    _openSlide(key) {
        if (this.state.showDescription !== key) {
            this.setState({ showDescription: key })
        } else {
            this.setState({ showDescription: "" })
        }
    }

    componentDidMount() {
        if (this.props.abos) {
            this.setState({ abosLoaded: true });
        }
    }

    render() {
        if (this.state.abosLoaded) {
            let abos = this.props.abos.abos
            console.log(abos)
            const cards = Object.keys(this.props.abos.abos).map((abo, i) => {
                let aboData = abos[abo]
                if(aboData.active) {
                    return (
                        <View key={aboData.title} style={stylesCard.content}>
                            <Card transparent style={stylesCard.card}>
                                <CardItem cardBody button onPress={() => this._openSlide(i)}>
                                    <ImageBackground source={{ uri: aboData.aboImage.url }} style={stylesCard.image}>
                                        <Text style={stylesFont.headerTextAbo}>{aboData.title}</Text>
                                    </ImageBackground>
                                </CardItem>
                                <SnackBar
                                    visible={(this.state.currentAboTitle == aboData.title) && this.state.showSnackBar}
                                    textMessage={this.state.snackBarMessage + this.state.currentAboTitle}
                                    backgroundColor="#FCEC45"
                                    messageColor="#707070"
                                    position="top"
                                />
    
                            </Card>
                            {(i === this.state.showDescription) &&
                                <CardItem style={stylesAbo.aboBeschreibung}>
                                    <Text style={stylesFont.textAbo}>
                                        {aboData.description}
                                    </Text>
                                </CardItem>
                            }
                            <View style={stylesCard.contentButton} marginTop={'-1.2%'}>
                                <Button
                                    style={this.props.isConnected ? stylesButton.buttonYellow : stylesButton.buttonOffline}
                                    onPress={() => this._onSubscribe(aboData.title, abo)}>
                                    {this.props.user.abos.includes(abo) ? <Text style={stylesButton.buttonTextCard}>Abonniert</Text> : <Text style={stylesButton.buttonTextCard}>Abonnieren</Text>}
                                </Button>
                            </View>
    
                        </View>
                    )

                }
            })
            return (
                <View style={{ flex: 1 }}>
                    <ScrollView contentContainerStyle={stylesCard.scrollView}>
                        {this.props.isConnected ? null : <Text style={stylesFont.textOffline}>Du bist Offline</Text>}
                        {cards}
                    </ScrollView>
                </View>
            );
        } else {
            return (null)
        }
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        abos: state.abos,
        user: state.user,
        isConnected: state.network.isConnected
    }
}

export default connect(mapStateToProps, {
    updateAbos,
    updateAbosUser,
    updateAboSlide
})(Abos);