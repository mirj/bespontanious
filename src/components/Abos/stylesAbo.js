import {StyleSheet} from 'react-native';

export default stylesAbo = StyleSheet.create({
    aboBeschreibung:{
        marginTop: '-4%',
        borderRadius: 5,
        marginBottom: '2.5%',
        zIndex: -1,
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 4,
            },
        shadowOpacity: 0.32,
        shadowRadius: 5.5,
        elevation: 9,
    },
    });