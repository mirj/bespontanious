import React from 'react';
import { Icon, View, Left, Right, Row } from 'native-base';
import stylesAppIcons from './stylesAppIcons';

/**
 * Header buttons to navigate to profile or abos
 */
export default class AppHeaderIcons extends React.Component {
    render() {
        return (
            <View>
                <Row>
                    <Left>
                        <Icon
                            style={[stylesAppIcons.icon, (this.props.navigation.state.routeName == 'Abo') ? stylesAppIcons.iconActive : ""]}
                            type="Entypo"
                            name="archive"
                            onPress={() => this.props.navigation.navigate('Abo')} />
                    </Left>
                    <Right>
                        <Icon style={[stylesAppIcons.iconRight, (this.props.navigation.state.routeName == 'Profile') ? stylesAppIcons.iconActive : ""]}
                            type="FontAwesome"
                            name="user"
                            onPress={() => this.props.navigation.navigate('Profile')} />
                    </Right>
                </Row>
            </View>
        );
    }
}