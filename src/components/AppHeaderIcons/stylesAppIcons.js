import {StyleSheet} from 'react-native';

export default stylesAppIcons = StyleSheet.create({
    icon: { 
        marginRight: 30, 
        color: '#707070' 
    },
    iconRight: {
        marginRight: 20,
        color: '#707070' 
    },
    iconActive: {
        color: '#006E89'
    }
});