import {Platform, StyleSheet} from 'react-native';

export default stylesHeader = StyleSheet.create({
    logo: {
        ...Platform.select({
            ios: {
                width: 65,
                height: 65,
                marginLeft: 10,
                marginTop: 20,
            },
            android: {
              marginLeft: 10,
              width: 45, 
              height: 45
            },
        })
    },
    title: {
        textAlign: 'center',
    },
    backButton: {
        marginLeft: 10
    }
});