import React from 'react';
import {Image} from 'react-native';
import { Icon, View, Left, Right, Row } from 'native-base';
import { NavigationActions } from 'react-navigation';

/**
 * Component for the header icons on the left (logo and backbutton)
 */
export default class AppHeaderIconsLeft extends React.Component {
    render() {
        return (
            <View>
                <Row>
                    <Left>
                        <Icon style={stylesHeader.backButton} 
                              name="arrow-back" 
                              onPress={() => this.props.navigation.dispatch(NavigationActions.back())} />
                    </Left>
                    <Right>
                        <Image style={stylesHeader.logo} source={require('../../../assets/images/316_bbs_Logo_rgb.png')} />
                    </Right>
                </Row>
            </View>
        );
    }
}