import React, { Component } from 'react';
import { Text, View, Icon } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import stylesUser from '../User/stylesUser';

/**
 * Component to show the awards of the user
 */

class Awards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            awards: [],
        }
    }

    componentDidMount() {
        // Get data from firebase
        this.getAwards();
    }

    /**
     * Get the awards form firebase
     */
    getAwards() {
        let pointsRef = firebase.database().ref('users/' + this.props.user.uid + '/awards')
        pointsRef.on('value', (snapshot) => {
            this.setState({ awards: snapshot.val() })
        });
    }

    render() {
        let awards
        if (this.state.awards) {
            awards = this.state.awards.map((award, i) => {
                return (
                    <View key={i} style={stylesUser.awardContainer}>
                        <Icon style={stylesUser.iconAward} fontSize="30" color="#FCEC45"  name="md-trophy" />
                        <Text style={stylesUser.textAward}>{award.title}</Text>
                    </View>
                )
            })
        } else {
            awards = null
        }

        return (
            <View style={stylesUser.containerAward}>
                {awards}
            </View>
        )
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        user: state.user
    }
}

/*Connects Component to Redux Store */
export default connect(mapStateToProps)(Awards);