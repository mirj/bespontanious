import React, { Component } from 'react';
import { Container, Form, Item, Input, Button, Text, View } from 'native-base';
import { Alert, Image } from 'react-native';
import { connect } from 'react-redux';
import errorHandling from '../../constants/errorFunction';
import * as firebase from 'firebase';
import stylesForgotPassword from './stylesForgotPassword';
import stylesButton  from '../../constants/Styling/stylesButton';

/**
 * Component to change password
 */
class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mail: "", 
      error: ""
    }
  }

  /**
   * Submit password reset request to firebase.
   */
  _onForgotPassword() {
    firebase.auth().sendPasswordResetEmail(this.state.mail).then(() => {
        /* Show password reset success message */
        Alert.alert(
            'E-Mail versendet',
            'Das Passwort wurde zurückgesetzt, ein E-Mail wurde an '+this.state.mail+' gesendet.',
            [{text: 'Zurück zum Login', onPress:() => this.props.navigation.navigate('Login')},],
            {cancelable: false}
          )
      })
      /*calls error handling function when not successfull*/
      .catch(error => this.setState({error: errorHandling(error.code)}))
  }

  render() {
    return (
      <Container>
        <View style={stylesForgotPassword.background}>
        <Image style={stylesForgotPassword.logo} source={require('../../../assets/images/316_bbs_Logo_rgb.png')}/>
            <Form>
                <Item>
                <Input style={stylesForgotPassword.textMail} placeholder="E-Mail" name="mail" 
                    onChangeText={(mail) => this.setState({mail})}
                    value={this.state.mail}/>
                </Item>
                <View style={stylesLogin.buttonMargin}>
                <Button style={stylesButton.buttonWhite} onPress={() => this._onForgotPassword()}>
                    <Text style={stylesButton.buttonText}>Passwort zurücksetzen</Text>
                </Button>
                </View>
                <Text>{this.state.error}</Text>
            </Form>
        </View>
      </Container>
    );
  }
}
/*Connects Component to Redux Store */
export default connect()(ForgotPassword);