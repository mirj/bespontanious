import {StyleSheet} from 'react-native';

export default stylesForgotPassword = StyleSheet.create({
    background:{
        flex: 1,
        position: 'absolute',
        backgroundColor: '#00313D',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonMargin:{
        marginTop: 50,
    },
    logo:{
        width: 120,
        height: 110,
        marginBottom: 80,
    },
    textMail:{
        color: '#fff',
    },
});