import React from 'react';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { ScrollView } from 'react-native';
import { Text, View, Form, Item, Input, Button, Container, Spinner } from 'native-base';
import stylesButton from '../../constants/Styling/stylesButton';
import stylesBackground from '../../constants/Styling/stylesBackground';
import stylesErrorMessage from '../../constants/Styling/stylesErrorMessage';
import stylesFont from '../../constants/Styling/stylesFont';
import stylesGenerator from './stylesGenerator';

/**
 * Component for the random generator
 */

class Generator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                1: "",
                2: "",
                3: "",
                4: ""
            },
            choosenOption: "",
            optionGenerated: false,
            generating: false
        }
    }

    /**
     * Saves the provided options into an array and calls function to choose option.
     */
    _onStartGenerator() {
        let options = [];
        this.setState({ generating: true });

        // Filter out any empty option feilds.
        for (let i = 1; i < 5; i++) {
            if (this.state.options[i]) {
                options.push(this.state.options[i])
            }
        }

        if (options.length > 1) {
            this.chooseOption(options);
        } else {
            this.setState({ errorMessage: "Bitte gib mindestens zwei Möglichkeiten ein." });
        }
    }

    /**
     * Randomly choose one of the given options. Start loading animation and update firebase.
     * @param {*} options 
     */
    chooseOption(options) {
        let randomNumber = Math.floor(Math.random() * options.length);
        this.setState({ choosenOption: options[randomNumber], optionGenerated: true });
        this.setState({ options: { 1: "", 2: "", 3: "", 4: "" } });
        
        // Disable genrating animation after rand int between 2500 and 4000.
        setTimeout(() => {
            this.setState({ generating: false, errorMessage: "" });
        }, this.getRandomInt(2500, 4000));

        // Mark the generator as a used feature (for awards)
        firebase.database().ref('/users/' + this.props.user.uid + '/featuresUsed/').update({
            generator: true
        });
    }

    /**
     * gets random int between to numbers
     * @param {*Integer} min 
     * @param {*Integer} max 
     */
    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    render() {
        if (!this.state.optionGenerated) {
            return (
                <Container style={{flex: 1, justifyContent:"flex-end"}}>
                    <View style={stylesBackground.backgroundColor}>
                        <Text style={stylesFont.headerGenerator}>Zufallsgenerator</Text>
                        <Text style={stylesFont.textGenerator}> Du kannst dich nicht entscheiden? Oder willst du einfach etwas mehr Zufall erleben?
                                Fülle deine Optionen ein und der Zufallsgenerator trifft für dich die Entscheidung.</Text>

                            <Item style={stylesGenerator.box}>
                                <Input style={stylesGenerator.boxText} placeholder="Option 1" onChangeText={(option1) => {
                                    let options = Object.assign({}, this.state.options);
                                    options[1] = option1;
                                    this.setState({ options });
                                }} />
                            </Item>
                            <Item style={stylesGenerator.box}>
                                <Input style={stylesGenerator.boxText} placeholder="Option 2" onChangeText={(option2) => {
                                    let options = Object.assign({}, this.state.options);
                                    options[2] = option2;
                                    this.setState({ options });
                                }} />
                            </Item>
                            <Item style={stylesGenerator.box}>
                                <Input style={stylesGenerator.boxText} placeholder="Option 3" onChangeText={(option3) => {
                                    let options = Object.assign({}, this.state.options);
                                    options[3] = option3;
                                    this.setState({ options });
                                }} />
                            </Item>
                            <Item style={stylesGenerator.box}>
                                <Input style={stylesGenerator.boxText} placeholder="Option 4" onChangeText={(option4) => {
                                    let options = Object.assign({}, this.state.options);
                                    options[4] = option4;
                                    this.setState({ options });
                                }} />
                            </Item>
                        <View style={stylesGenerator.contentError}>
                        <Text style={stylesErrorMessage.errorMessageYellow}>{this.state.errorMessage}</Text></View>
                        <View style={stylesGenerator.contentButton}>
                            <Button style={stylesButton.buttonYellow} marginTop={30} marginBottom={10} onPress={() => this._onStartGenerator()}>
                                <Text style={stylesButton.buttonText}>Start</Text>
                            </Button>
                        </View>
                    </View>
                </Container>
            )
        }
        if (this.state.optionGenerated) {
            return (
                <Container>
                    {!this.state.generating &&
                        <View style={stylesBackground.backgroundColor}>
                            <View style={stylesGenerator.containerResult}>
                                <Text style={stylesFont.textResultGenerator}>
                                    {this.state.choosenOption}
                                </Text>
                                <Button style={stylesButton.buttonYellow} onPress={() => this.setState({ optionGenerated: false })}>
                                    <Text style={stylesButton.buttonText}>Zurück zum Zufallsgenerator</Text>
                                </Button>
                            </View>
                        </View>
                    }
                    {this.state.generating &&
                        <View style={stylesGenerator.waitingScreen}>
                            <Text style={stylesGenerator.waitingText}>Deine Option wird ausgesucht...</Text>
                            <Spinner color='#FCEC45' />
                        </View>
                    }
                </Container>
            )
        }
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        user: state.user,
    }
}

export default connect(mapStateToProps, {})(Generator);