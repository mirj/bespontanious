import { StyleSheet } from 'react-native';

export default stylesGenerator = StyleSheet.create({
    content: {
        width: '90%',
        alignSelf: 'center',
        marginBottom: 40,

    },
    form: {
        paddingTop: 20,
    },
    box: {
        backgroundColor: '#EBEBEB',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,

    },
    boxText: {
        textAlign: 'center',
        fontSize: 16,
    },
    contentButton: {
        width: '70%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    containerResult: {
        width: '90%',
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    contentError: {
        marginLeft: 10,
        marginRight: 10,
        justifyContent: 'center',
        marginBottom: 10,
    },
    waitingScreen: {
        backgroundColor: '#00313D',
        flex: 1,
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40
    },
    waitingText: {
        fontSize: 37,
        fontWeight: 'bold',
        color: '#fff',
        textAlign: 'center'
    }
});