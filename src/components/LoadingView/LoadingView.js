import React from 'react';
import { Text, View } from 'native-base';
import { Image } from 'react-native';
import stylesLoadingView from './stylesLoadingView';

/**
 * Loading component
 */
export default class LoadingView extends React.Component {
    render() {
        return (
            <View style={stylesLoadingView.container}>
                <Image style={stylesLoadingView.logo} source={require('../../../assets/images/316_bbs_Logo_rgb.png')} />
                <Text style={stylesLoadingView.text}>Loading...</Text>
            </View>
        );
    }
}