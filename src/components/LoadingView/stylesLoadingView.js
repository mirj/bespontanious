import {StyleSheet} from 'react-native';

export default stylesLoadingView = StyleSheet.create({
    text:{
        textAlign: 'center',
        color: '#Fff',
        fontSize: 40,
        fontWeight: 'bold',
        marginTop: 50
    },
    container: {
        flex: 1, 
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00313D'
    },
    logo: {
        height: 200,
        width: 200
    }
});