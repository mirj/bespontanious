import React, { Component } from 'react';
import { Container, Form, Item, Input, Button, Text, Spinner, Icon } from 'native-base';
import { StyleSheet, View, ImageBackground, BackHandler } from 'react-native'
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import errorHandling from '../../constants/errorFunction';
import stylesLogin from './stylesLogin';
import stylesButton from '../../constants/Styling/stylesButton';
import {
  updateUIDUser,
  updateEmailUser,
  updateUsernameUser,
  updateBirthdayUser,
  updateAbosUser,
  updateTypUser,
  updateMomentsUser,
  updateUserPoints
} from '../../actions';
import { registerForPushNotificationsAsync } from '../../actions'

/* Initialize Firebase */
const firebaseConfig = {
  apiKey: "AIzaSyB6O1LN3e0wAW5ZcuAIXT_Dh0lk1plAXJo",
  authDomain: "bespontaneous-782bf.firebaseapp.com",
  databaseURL: "https://bespontaneous-782bf.firebaseio.com",
  projectId: "bespontaneous-782bf",
  storageBucket: "bespontaneous-782bf.appspot.com",
  messagingSenderId: "676636678226"
};

/* initialize Firebase DB */
firebase.initializeApp(firebaseConfig);

/**
 * Component for login
 */
class Login extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      loading: false,
      error: "",
      mailMissing: false,
      passwordMissing: false,
      passwordShort: false,
      passwordInputFocus: false
    }
  }

  componentWillMount() {
    //Eventlistener for backbutton on android
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    //remove eventlistener for backbutton on android
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  /**
   * Handels backbutton for android phones
   */
  handleBackButtonClick() {
    this.props.navigation.navigate('Welcome');
    return true;
  }

  /**
   * Login to firebase on login form submit
   */
  _onLogin() {
    this.setState({ loading: true });

    if (this._loginValidation()) {
      firebase.auth().signInWithEmailAndPassword(this.state.mail, this.state.password).then((user) => {
        this.setState({ loading: false });
        /* Add UserID to redux store */
        this.props.updateUIDUser(user.user.uid);
        this._fetchUserData(user.user.uid);
        this.props.navigation.navigate('App');
      })
        .catch(error => {
          this.setState({ loading: false });
          console.log(error.code)
          this.setState({ error: errorHandling(error.code) });
        })
    }
  }

  /**
   * Validate login form data.
   */
  _loginValidation() {
    let isValid = true;
    this.setState({ mailMissing: false, passwordMissing: false, passwordShort: false, error: "" });
    if (!this.state.mail) {
      isValid = false;
      this.setState({ loading: false, error: errorHandling('auth/no-required'), mailMissing: true });
    }

    if (!this.state.password) {
      isValid = false;
      this.setState({ loading: false, error: errorHandling('auth/no-required'), passwordMissing: true });
    }

    /* Password must be at least 6 chars */
    if (this.state.password && this.state.password.length < 6) {
      isValid = false;
      this.setState({ loading: false, error: errorHandling('auth/to-short-password'), passwordShort: true });
    }
    return isValid
  }

  /**
   * Login with Facebook.
   */
  async _onFacebookLogin() {
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync
      ('319890002076323', { permission: ['public_profile'] })

    if (type == 'success') {
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      firebase.auth().signInAndRetrieveDataWithCredential(credential).then((user) => {
        this.props.updateUIDUser(user.user.uid);
        this._fetchUserData(user.user.uid);
        this.props.navigation.navigate('App');
      })
        .catch(error => {
          this.setState({ loading: false });
          this.setState({ error: errorHandling(error.code) })
        })
    }
  }

  /**
    * Get user data from firebase. Take userId from store and add fetched data to store.
    */
  _fetchUserData(uid) {
    let fUser;
    firebase.database().ref('users/' + uid).once('value', function (snapshot) {
      fUser = snapshot.val();
    }).then(() => {
      this.props.updateEmailUser(fUser.email);
      this.props.updateUsernameUser(fUser.username);
      this.props.updateBirthdayUser(fUser.birthday);
      this.props.updateTypUser(fUser.typ);
      if (fUser.abos) this.props.updateAbosUser(fUser.abos);
      this.props.updateUIDUser(fUser.uid);
      this.props.updateMomentsUser(fUser.moments);
      this.props.updateUserPoints(fUser.points);
      registerForPushNotificationsAsync(fUser.uid)
    })
  }

  render() {
    return (
      <Container style={stylesLogin.backgroundComponent}>
        <Button style={stylesButton.backButton} marginTop={20} marginLeft={'8%'} onPress={() => this.handleBackButtonClick()}>
          <Icon name="arrow-back"></Icon>
          <Text style={stylesButton.backButtonText}>Zurück</Text>
        </Button>
        <Form style={stylesLogin.container}>
          <View style={stylesLogin.login}>
            {/* EMail Input */}
            <Item error={this.state.mailMissing} last>
              <Input style={stylesLogin.textLogin} placeholder="E-Mail" name="mail" required
                onChangeText={(mail) => this.setState({ mail })}
                returnKeyType={"next"}
                onSubmitEditing={() => this.setState({ passwordInputFocus: true })}
                keyboardType="email-address"
                value={this.state.mail} />
              {this.state.mailMissing && <Icon name='close-circle' />}
            </Item>

            {/* Password Input */}
            <Item error={this.state.passwordMissing || this.state.passwordShort} last>
              <Input style={stylesLogin.textLogin} placeholder="Passwort" name="password" secureTextEntry={true}
                focus={this.state.passwordInputFocus}
                required onChangeText={(password) => this.setState({ password })}
                onSubmitEditing={() => this._onLogin()}
                value={this.state.password} />
              {(this.state.passwordMissing || this.state.passwordShort) && <Icon name='close-circle' />}
            </Item>
            {/* Error message */}
            <Text style={stylesLogin.textLogin}>{this.state.error}</Text>
            {/* Submit */}
            <View style={stylesButton.buttonMargin}>
              <Button style={stylesButton.buttonWhite} onPress={() => this._onLogin()}>
                <Text style={stylesButton.buttonText}>Login</Text>
              </Button>
            </View>
            {/* Facebook Login */}
            <View style={stylesButton.buttonMargin}>
              <Button style={stylesButton.buttonFacebook} onPress={() => this._onFacebookLogin()}>
                <Icon style={stylesButton.buttonFacebookIcon} type="EvilIcons" name="sc-facebook" />
                <Text style={stylesButton.buttonFacebookText}>Login mit Facebook</Text>
              </Button>
            </View>
          </View>
          {/* Forgot Password */}
          <Text style={stylesLogin.forgotPassword} onPress={() => this.props.navigation.navigate('ForgotPassword')}>Passwort vergessen</Text>
        </Form>
        {/* Loading spinner */}
        {this.state.loading &&
          <View style={stylesLogin.loading}>
            <Spinner color='#FCEC45' />
          </View>
        }
      </Container>
    );
  }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, {
  updateEmailUser,
  updateUsernameUser,
  updateBirthdayUser,
  updateTypUser,
  updateUIDUser,
  updateAbosUser,
  updateUserPoints,
  updateMomentsUser
})(Login);