import { StyleSheet } from 'react-native';

export default stylesLogin = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: '100%',
        
    },
    backgroundComponent: {
        backgroundColor: 'transparent'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        
    },
    loading: {
        position: 'absolute',
        backgroundColor: '#F5FCFF88',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    login: {
        width: '80%',
    },
    forgotPassword: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        color: '#fff',
        fontSize: 16,
    },
    textLogin: {
        color: '#fff',
    },
    backButton:{
        width: '80%',
        marginLeft: '10%',
    }
});