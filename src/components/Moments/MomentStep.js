import React from 'react';
import { View } from 'react-native';
import { Card, CardItem, Text, Body } from 'native-base';
import stylesMoments from './stylesMoments';
import stylesCard from '../../constants/Styling/stylesCard';
import stylesFont from '../../constants/Styling/stylesFont';

/**
 * Component shows moment steps
 */
export default class MomentStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props,
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({ data: newProps });
    }

    render() {
        let newAwards
        if (this.state.data.user.newAwards) {
            newAwards = this.state.data.user.newAwards.map((award, i) => {
                return (
                    <View>
                        <Text style={stylesFont.momenteText}>Neuer Award</Text>
                        <Text style={stylesFont.momenteText}>{award.title}</Text>
                    </View>

                )
            })
        } else {
            newAwards = false
        }
        return (
            <View style={stylesCard.contentStep}>
                <Card transparent key={this.state.data.step} style={stylesCard.card}>
                    <CardItem style={[stylesMoments.container, stylesMoments.cardRadiusBottom]}>
                        <Text style={stylesFont.momenteHeader}>#{this.state.data.aboNames}</Text>
                    </CardItem>
                    <CardItem style={[stylesMoments.backgroundColorCard, stylesMoments.cardRadiusTop]}>
                        <Body style={stylesMoments.container}>
                            {!this.props.completed &&
                                <View>
                                    <Text style={stylesFont.momenteTitle}>{this.state.data.step.title}</Text>
                                    <Text style={stylesFont.momenteText}>{this.state.data.step.description}</Text>
                                    <Text style={stylesFont.momenteText}>{this.state.data.stepNumber}/{this.state.data.stepMax}</Text>
                                </View>
                            }
                            {this.props.completed &&
                                <View>
                                    <Text style={stylesFont.momenteTitle}>Abgeschlossen</Text>
                                    <Text style={stylesFont.momenteText}>Total Punkte: {this.state.data.user.points}</Text>
                                    <Text style={stylesFont.momenteText}>Erhaltene Punkte: {this.state.data.moment.points}</Text>
                                    {newAwards && newAwards}
                                </View>
                            }
                        </Body>
                    </CardItem>
                </Card>
            </View>
        )
    }
}