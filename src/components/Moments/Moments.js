import React from 'react';
import { Image, View, ScrollView } from 'react-native';
import { Card, CardItem, Button, Icon, Text, Body, Container, Right, Left, Row, Content } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { updateMomentsUser, updateMomentState, updateUserPoints, fakeConnectionChange, updateSeenModal } from '../../actions';
import stylesMoments from './stylesMoments';
import MomentStep from './MomentStep';
import stylesButton from '../../constants/Styling/stylesButton';
import stylesCard from '../../constants/Styling/stylesCard';
import stylesBackground from '../../constants/Styling/stylesBackground';
import Modal from "react-native-modal";
import stylesFont from '../../constants/Styling/stylesFont';
import stylesOverlay from '../../constants/Styling/stylesOverlay';

/**
 * Component for the moments
 */

class Moments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startedMoment: false,
            step: 1,
            activeMoment: {},
            activeMomentUid: "",
            rejectedModal: false,
            rejectedMoment: "",
            isSynchronized: true,
            isConn: false,
            momentsChanged: false
        }
    }

    componentDidMount() {
        // Get data from firebase
        this.props.navigation.addListener('willFocus', (route) => {
            this.setSeenFlag();
        });
    }

    /**
     * to rerender when props are changed
     * @param {*} prevProps 
     */
    componentDidUpdate(prevProps) {
        if (!(this.props.user.seenModal === prevProps.user.seenModal)) 
        {
            this.setSeenFlag();
            this.props.updateSeenModal(false);
        }
        if (!(this.props.user.moments === prevProps.user.moments)) {
            this.setState({ momentsChanged: true });
        }
    }

    /**
     * set seen flag on firebase when user has seen moment
     */
    setSeenFlag() {
        var updateData = {
            seen: true
        }
        var ref = firebase.database().ref();
        ref.child('users/' + this.props.user.uid + '/moments').orderByChild('/seen').equalTo(false).once("value", function (snapshot) {
            snapshot.forEach(function (child) {
                child.ref.update(updateData);
            });
        });
    }

    /**
     * updates moment status on firebase
     * @param {*} momentUid id of moment which should be updated
     * @param {*} status 
     */
    updateMomentStatus(momentUid, status) {
        if (this.props.user.momentsSynchronised) {
            firebase.database().ref('/users/' + this.props.user.uid + '/moments/' + momentUid).update({
                status: status
            });
        }
    }

    /**
     * updates user points on firebase
     * @param {*} points 
     */
    updateUserPoints(points) {
        var ref = firebase.database().ref('users/' + this.props.user.uid + '/points');
        ref.transaction(function (currentPoints) {
            // If node/clicks has never been set, currentRank will be `null`.
            return (currentPoints || 0) + points;
        });
    }

    /**
     * Go to next step in currently selected moment
     */
    _nextStep() {
        this.setState({ step: this.state.step + 1 });

        if (this.state.activeMoment.steps.length === this.state.step) {
            this._onMomentComplete(this.state.activeMoment)
        } else if (this.state.activeMoment.steps.length < this.state.step) {
            this.setState({ startedMoment: false, step: 1, activeMoment: {} });
        }
    }

    /**
     * Go to previous step in currently selected moment
     */
    stepBack() {
        if (1 < this.state.step) {
            this.setState({ step: this.state.step - 1 });
        } else if (this.state.step === 1) {
            this.setState({ startedMoment: false, step: 1, activeMoment: {} });
        }
    }

    /**
     * When a moment is complete. Update its status locally and on firebase
     * @param {Object} moment 
     */
    _onMomentComplete(moment) {
        this.props.updateMomentState({ "status": 3, "uid": this.state.activeMomentUid });
        this.updateMomentStatus(this.state.activeMomentUid, 3);
        this.props.updateUserPoints(moment.points);
        this.updateUserPoints(moment.points);
    }

    /**
     * When moment gets rejected show rejected modal
     * @param {Integer} momentUid 
     */
    _onReject(momentUid) {
        this.setState({ rejectedModal: true, rejectedMoment: momentUid });
    }

    /**
     * When moment is accepted show momentsteps
     * @param {Object} moment 
     */
    _onAccept(moment) {
        this.props.updateMomentState({ "status": 2, "uid": moment.uid });
        this.updateMomentStatus(moment.uid, 2);
        this.setState({ activeMoment: moment.data, startedMoment: true, activeMomentUid: moment.uid });
    }

    /**
     * When beeing-sure modul is rejected, remove buttons from moment and change style of moment
     */
    _onConfirmingRejection() {
        this.props.updateMomentState({ "status": 1, "uid": this.state.rejectedMoment });
        if (this.state.user.momentsSynchronised) this.updateMomentStatus(this.state.rejectedMoment, 1);
        this.setState({ rejectedModal: false, rejectedMoment: {} });
    }


    render() {
        if (this.props.user.moments) {
            let moments = [];
            //Save the Moments into an Array
            for (var moment in this.props.user.moments) {
                if (this.props.user.moments.hasOwnProperty(moment)) {
                    let thisMoment = this.props.user.moments[moment];
                    let abosMoment;
                    let aboNames = [];

                    if (thisMoment.abos) {
                        abosMoment = Object.keys(thisMoment.abos)
                        abosMoment.forEach(aboMoment => {
                            Object.entries(this.props.abos.abos).map((abo) => {
                                if (abo) {
                                    if (abo[0] === aboMoment) {
                                        aboNames.push(abo[1].title);
                                    }
                                }
                            })
                        });
                    } else {
                        aboNames.push("tutorial");
                    }

                    thisMoment.aboNames = aboNames.join(", #");
                    moments.push({ "uid": moment, "data": thisMoment })
                }
            }
            moments.reverse();
            firstMoments = moments.slice(0, 10);

            //Loop through the array of moments and render components
            const cards = firstMoments.map((moment, i) => {
                return (
                    <View key={i} style={stylesCard.content}>
                        <Card style={stylesCard.card}>
                            <CardItem style={stylesMoments.container}>
                                <Text style={stylesFont.momenteHeader}>#{moment.data.aboNames}</Text>
                            </CardItem>
                            <CardItem cardBody>
                                <Body style={[stylesMoments.container, stylesMoments.cardRadiusTop, stylesMoments.backgroundColorCard, (moment.data.status == 1) ? stylesMoments.colorRejected : "", (moment.data.status == 3) ? stylesMoments.colorDone : ""]}>
                                        <Text style={stylesFont.momenteTitle}>{moment.data.title}</Text>
                                    <Text style={stylesFont.momenteText}>{moment.data.description}</Text>
                                </Body>
                            </CardItem>
                        </Card>
                        {((moment.data.status == 0) || (moment.data.status == 2)) &&
                            <Row>
                                <Left style={stylesCard.contentButtonMomente}>
                                    <Button onPress={() => this._onReject(moment.uid)} style={stylesButton.buttonWhiteMomente}>
                                        <Text style={stylesButton.buttonTextCard}>Nein Danke</Text>
                                    </Button>
                                </Left>
                                <Right style={stylesCard.contentButtonMomente}>
                                    <Button onPress={() => this._onAccept(moment)} style={stylesButton.buttonGrayMomente}>
                                        <Text style={stylesButton.buttonTextCard}>Mach ich</Text>
                                    </Button>

                                </Right>
                            </Row>
                        }
                    </View>
                )
            })
            /**
             * Show steps when moment is started
             */
            if (this.state.startedMoment) {
                return (
                    <Container>
                        <View style={stylesBackground.backgroundColor} paddingTop={30}>
                            <MomentStep
                                step={this.state.activeMoment.steps[this.state.step - 1]}
                                stepNumber={this.state.step}
                                aboNames={this.state.activeMoment.aboNames}
                                stepMax={this.state.activeMoment.steps.length}
                                moment={this.state.activeMoment}
                                user={this.props.user}
                                completed={this.state.activeMoment.steps.length === this.state.step - 1} >
                            </MomentStep>
                            <Row>
                                <Left style={stylesCard.contentButtonMomenteSteps}>
                                    <Button style={stylesButton.buttonWhiteMomente} onPress={() => this.stepBack()}>
                                        <Text style={stylesButton.buttonTextCard}>Zurück</Text>
                                    </Button>
                                </Left>
                                <Right style={stylesCard.contentButtonMomenteSteps}>
                                    <Button style={stylesButton.buttonGrayMomente} onPress={() => this._nextStep()}>
                                        <Text style={stylesButton.buttonTextCard}>Weiter</Text>
                                    </Button>
                                </Right>
                            </Row>
                        </View>
                    </Container>
                )
            } 
            /**
             * Show all the moment when no moment is started
             */
            else {
                return (
                    <ScrollView style={stylesCard.scrollView}>
                        {cards}
                        <View>
                            <Modal isVisible={this.state.rejectedModal} style={stylesOverlay.contentOverlay}>
                                <Icon style={stylesOverlay.iconOverlay} iconRight light onPress={() => this.setState({ rejectedModal: false })} name='ios-close' />
                                <Image style={stylesOverlay.imageOverlay} source={require('../../../assets/images/crying.png')} />
                                <View style={stylesOverlay.containerOverlay}>
                                    <Text style={stylesOverlay.textOverlay}>Bist du sicher, dass du diesen Moment nicht machen möchtest?</Text>
                                </View>
                                <View style={stylesCard.contentButton}>
                                    <Button style={stylesButton.buttonYellow} onPress={() => this._onConfirmingRejection()} >
                                        <Text style={stylesButton.buttonText}>Ja, nicht machen!</Text>
                                    </Button>
                                </View>
                            </Modal>
                        </View>
                    </ScrollView>
                )
            }
        } 
        /**
         * When there is no moment show message for user
         */
        else {
            return (
                <View style={stylesMoments.contentNoMoment}>
                    <Text style={stylesErrorMessage.errorMessageYellow}>
                        Wir haben dir noch keine Momente zugesendet. Abonniere dir mindestens eine Sehnsucht, damit du Momente erhälst.
                </Text>
                </View>
            )

        }
    }

}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        user: state.user,
        abos: state.abos,
        isConnected: state.network.isConnected,
    }
}

export default connect(mapStateToProps, {
    updateMomentsUser,
    updateMomentState,
    updateUserPoints,
    fakeConnectionChange,
    updateSeenModal
})(Moments);