import {StyleSheet} from 'react-native';
import { Right } from 'native-base';

export default stylesMoments = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        borderRadius: 5
    },
    backgroundColorCard:{
        backgroundColor: '#FCEC45'
    },
    colorRejected:{
        backgroundColor: '#EBEBEB'
    },
    colorDone: {
        backgroundColor: '#FFFFFF'
    },
    contentNoMoment:{
        marginLeft: 10,
        marginRight: 10,
        justifyContent: 'center',
        marginTop: 10,
    },
    cardRadiusTop:{
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
    },
    cardRadiusBottom:{
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
});