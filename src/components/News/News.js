import React from 'react';
import { ImageBackground, ScrollView, Image } from 'react-native';
import { Card, CardItem, Button, Icon, Text, View } from 'native-base';
import { connect } from 'react-redux';
import { updateNews, updateSeenModal } from '../../actions';
import stylesNews from './stylesNews';
import stylesButton from '../../constants/Styling/stylesButton';
import stylesCard from '../../constants/Styling/stylesCard';
import stylesFont from '../../constants/Styling/stylesFont';
import { WebBrowser } from 'expo';
import Modal from "react-native-modal";
import { showMessage } from "react-native-flash-message";

/**
 * Component to show news
 */
class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newsLoaded: false,
            newMomentModal: false
        }
    }

    componentDidUpdate(prevProps) {
        //Checks if there is a new notification, to show badger in tabbar
        if (!(this.props.user.notification <= prevProps.user.notification)) {
            this.updateNotification();
        }
    }

    /**
     * Opens the Browser and redirects to the Blogarticle on the Website
     */
    _openArticle = async (uri) => {
        if (!this.props.isConnected) {
            showMessage({
                message: "Keine verbindung",
                description: "Diese Funktion ist offline nicht verfügbar",
                type: "default",
                backgroundColor: "#FCEC45", // background color
                color: "#707070"
            });
        } else {
            let result = await WebBrowser.openBrowserAsync(uri);
            this.setState({ result });
        }
    }

    /**
     * When there is a new moment it shows the modal
     */
    updateNotification() {
        if (this.props.user.notification > 0) {
            this.setState({ newMomentModal: true })
        }
    }
    /**
     * Fetches the News Data when component is loaded.
     */
    componentDidMount() {
        if (this.props.news) {
            this.setState({ newsLoaded: true });
        }
    }

    render() {
        if (this.state.newsLoaded) {
            const cards = Object.entries(this.props.news.news).map((news, i) => {
                return (
                    <View key={i} style={stylesCard.content}>
                        <Card transparent style={stylesCard.card}>
                            <CardItem cardBody>
                                <ImageBackground source={{ uri: news[1].featureImage.url }} style={stylesCard.image}>
                                    <View style={stylesNews.contentTitel}>
                                        <Text style={stylesFont.headerTextNews}>{news[1].title}</Text>
                                    </View>
                                </ImageBackground>
                            </CardItem>
                            <CardItem style={stylesNews.cardRadiusTop}>
                                <Text style={stylesFont.text}>{news[1].preview}</Text>
                            </CardItem>
                        </Card>
                        {/*Only shows button lesen when it has a link to article*/}
                        <View style={stylesCard.contentButton}>
                            {news[1].link &&
                                <Button style={this.props.isConnected ? stylesButton.buttonYellow : stylesButton.buttonOffline} onPress={() => this._openArticle(news[1].link)}>
                                    <Text style={stylesButton.buttonTextCard}>
                                        Lesen
                                </Text>
                                </Button>}
                        </View>
                    </View>
                )
            }
            )
            return (
                <ScrollView style={stylesCard.scrollView}>
                    {cards}
                    <View>
                        <Modal isVisible={this.state.newMomentModal} style={stylesOverlay.contentOverlay}>
                            <Icon style={stylesOverlay.iconOverlay} iconRight light onPress={() => this.setState({ newMomentModal: false })} name='ios-close' />
                            <Image style={stylesOverlay.imageOverlay} source={require('../../../assets/images/neuer-Moment.png')} />
                            <View style={stylesOverlay.containerOverlay}>
                                <Text style={stylesOverlay.textOverlay}>Neuer Moment für dich verfügbar</Text>
                            </View>
                            <View style={stylesCard.contentButton}>
                                <Button style={stylesButton.buttonYellow} onPress={() => {
                                    this.setState({ newMomentModal: false })
                                    this.props.updateSeenModal(true);
                                    this.props.navigation.navigate('Moments')
                                }} >
                                    <Text style={stylesButton.buttonText}>Zu den Momenten</Text>
                                </Button>
                            </View>
                        </Modal>
                    </View>
                </ScrollView>
            );
        } else {
            return (null)
        }
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        news: state.news,
        user: state.user,
        isConnected: state.network.isConnected
    }
}

export default connect(mapStateToProps, {
    updateNews,
    updateSeenModal
})(News);