import {StyleSheet} from 'react-native';

export default stylesNews = StyleSheet.create({
    contentTitel:{
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center'
    },
    cardRadiusTop:{
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
    },
});