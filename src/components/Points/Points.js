import React, { Component } from 'react';
import { Text, View } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { Icon } from 'react-native-elements';
import stylesUser from '../User/stylesUser';

/**
 * Component to show points
 */
class ChallengePoints extends Component {
    constructor(props) {
        super(props);
        this.state = {
            points: 0,
        }
    }

    componentDidMount() {
        // Get data from firebase
        this.getPoints();
    }

    /**
     * Get points from the user on firebase
     */
    getPoints() {
        let pointsRef = firebase.database().ref('users/' + this.props.user.uid + '/points')
        pointsRef.on('value', (snapshot) => {
            this.setState({points: snapshot.val() ? snapshot.val() : 0})
        });
    }

    render() {
        return (
            <View style={stylesUser.iconTextContainer}>
                <Icon fontSize="30" color="#FCEC45"  type="material-community" name="crown" />
                <Text style={stylesUser.text}>
                    {this.state.points}
                </Text>
            </View>
        );
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        user: state.user
    }
}

/*Connects Component to Redux Store */
export default connect(mapStateToProps)(ChallengePoints);