import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Form, Item, Input, Text, DatePicker, Icon } from 'native-base';
import { connect } from 'react-redux';
import { Card } from 'react-native-elements'
import { regSteps, regStepTitles } from '../../constants/General'
import stylesRegistrationStep from './stylesRegistrationStep';
import TypSlider from '../../components/TypeSlider/typSlider'
import CantonPicker from '../../components/canton/CantonPicker'
import stylesErrorMessage from '../../constants/Styling/stylesErrorMessage';

import {
    updateEmailRegistration,
    updatePasswordRegistration,
    updateUsernameRegistration,
    updateBirthdayRegistration,
    updateTypRegistration,
    setRegistrationStep,
    updateCantonRegistration
} from '../../actions';

/**
 * Components to show the different registrationstep
 */
class RegistrationStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            passwordInputFocus: false,
            typ: 1,
        }
    }

    render() {
        let errors
        //Show the validation errors when there are some
        if (this.props.registration.error.length) {
            errors = this.props.registration.error.map((error, i) => {
                return (
                    <View key={error}>
                        <Text style={stylesErrorMessage.errorMessage}>{error}</Text>
                    </View>
                )
            })
        }
        
        return (
            <View style={stylesRegistrationStep.container}>
                <View style={stylesRegistrationStep.logoView}>
                    <Image style={stylesRegistrationStep.logo} source={require('../../../assets/images/316_bbs_Logo_rgb.png')} />
                </View>
                <Card borderRadius={10} marginLeft={0} marginRight={0} paddingBottom={40}>
                    <Text style={stylesRegistrationStep.title}>{regStepTitles[this.props.registration.registrationStep]}</Text>
                    <View>
                        {this.props.registration.registrationStep == regSteps.emailPassword &&
                            <Form>
                                {/* Email Input */}
                                <Item error={this.props.registration.emailMissing}>
                                    <Input placeholder="E-Mail" name="mail"
                                        keyboardType="email-address"
                                        onChangeText={(mail) => this.props.updateEmailRegistration(mail)}
                                        returnKeyType={"next"}
                                        onSubmitEditing={() => this.setState({ passwordInputFocus: true })}
                                        value={this.props.registration.email} />
                                    {this.props.registration.emailMissing &&
                                        <Icon name='close-circle' />
                                    }
                                </Item>
                                {/* Password Input */}
                                <Item error={this.props.registration.passwordMissing || this.props.registration.passwordShort}>
                                    <Input placeholder="Passwort" name="password" secureTextEntry={true}
                                        focus={this.state.passwordInputFocus}
                                        onChangeText={(password) => this.props.updatePasswordRegistration(password)}
                                        value={this.props.registration.password} />
                                    {this.props.registration.passwordMissing &&
                                        <Icon name='close-circle' />
                                    }
                                    {this.props.registration.passwordShort &&
                                        <Icon name='close-circle' />
                                    }
                                </Item>
                            </Form>
                        }
                        {this.props.registration.registrationStep == regSteps.username &&
                            <Form>
                                {/* Username Input */}
                                <Item error={this.props.registration.usernameMissing}>
                                    <Input placeholder="Username" name="username"
                                        onChangeText={(username) => this.props.updateUsernameRegistration(username)}
                                        onSubmitEditing={() => this.props.setRegistrationStep(regSteps.birthday)}
                                        value={this.props.registration.username} />
                                    {this.props.registration.usernameMissing &&
                                        <Icon name='close-circle' />
                                    }
                                </Item>
                            </Form>
                        }
                        {/* Date of Birth Input */}
                        {this.props.registration.registrationStep == regSteps.birthday &&
                            <DatePicker
                                error={this.props.registration.birthdayMissing}
                                defaultDate={new Date(2018, 1, 1)}
                                minimumDate={new Date(1900, 1, 1)}
                                maximumDate={new Date(2018, 12, 31)}
                                locale={"ch"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"spinner"}
                                placeHolderText="Wähle ein Datum"
                                textStyle={{ color: "#00313D", textAlign: 'center' }}
                                placeHolderTextStyle={{ color: "#707070", textAlign: 'center' }}
                                onDateChange={(date) => this.props.updateBirthdayRegistration(date)}
                                onSubmitEditing={() => this.props.setRegistrationStep(regSteps.typ)}
                            />
                        }
                        {/* Typ selector */}
                        {this.props.registration.registrationStep == regSteps.typ &&
                            <TypSlider />
                        }
                        {this.props.registration.registrationStep == regSteps.canton &&
                            <CantonPicker registrationForm={true}/>
                        }
                        {errors}
                    </View>
                </Card>
            </View>
        );
    }
}
const mapStatetoProps = state => {
    return {
        registration: state.registration
    };
};
export default connect(mapStatetoProps, {
    updateEmailRegistration,
    updatePasswordRegistration,
    updateUsernameRegistration,
    updateBirthdayRegistration,
    updateTypRegistration,
    setRegistrationStep,
    updateCantonRegistration
})(RegistrationStep);