import { StyleSheet, Dimensions } from 'react-native';

export default stylesRegistrationStep = StyleSheet.create({
    logo: {
        width: 120,
        height: 120,
    },
    logoView: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        position: 'absolute',
        zIndex: 200,
        elevation: 7,
        top: -60,
    },
    container: {
        flex: 1,
        marginTop: 80,
        marginBottom: 25
    },
    title: {
        textAlign: 'center',
        color: '#001011',
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 50,
        paddingBottom: 15,
    },
    card: {
        borderRadius: 5,
        marginLeft: 0,
        marginRight: 0,
        paddingBottom: 30
    },
    slider:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageSpontiDna:{
        resizeMode: 'contain',
        height: 110,
        flex: 1,
        marginBottom: 15,
        backgroundColor: '#fff',
        
    },
    textSpontiDna:{
        textAlign: 'center',
        paddingTop: 10,
        color: '#001011',
        fontSize: 16,
        fontWeight: 'bold',
    },
    descriptionSpontiDna:{
        textAlign: 'center',
        paddingTop: 10,
        color: '#001011',
        fontSize: 16,
    },

});