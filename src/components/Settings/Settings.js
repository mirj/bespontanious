import React, { Component } from 'react';
import { Container, Text, Left, Right, Input, Button, Card, Form, Item, Icon, Body, Grid, Row, View, CardItem } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { Image, Slider, ScrollView } from 'react-native';
import stylesSettings from './stylesSettings';
import stylesIcons from '../../constants/Styling/stylesIcons';
import stylesButton from '../../constants/Styling/stylesButton';
import stylesFont from '../../constants/Styling/stylesFont';
import { clearUser, updateTypUser, updateUsernameUser } from '../../actions';
import email from 'react-native-email'
import { WebBrowser } from 'expo';
import errorHandling from '../../constants/errorFunction';
import CantonPicker from '../canton/CantonPicker';

/**
 * Component to show settings of user
 */
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userSettings: false,
      oldPassword: "",
      newPassword: "",
      newPasswordRepeat: "",
      newUsername: this.props.user.username,
      typ: this.props.user.typ,
      typText: this.props.user.typ.name,
      typImage: this.props.user.typ.avatarImage.url,
      errorMessagePassword: "",
      passwordChanged: false,
      errorMessageUsername: "",
      typs: {}
    }
  }

  componentDidMount() {
    this.getTyps();
  }

  componentWillUnmount() {
    this.setState({
      errorMessagePassword: "",
      errorMessageUsername: "",
    })
  }

  /**
   * Logout and navigate to Login
   */
  _onLogout() {
    firebase.auth().signOut()
      .then(() => {
        this.props.navigation.navigate('Welcome');
      })
      .catch((errorResponse) => {
        console.log(errorResponse)
      })
  }

  /**
   * Opens the Browser and redirects to the privacy articles on the Website
   * needs to be called with an uri (link to website)
   */
  _openLink = async (uri) => {
    let result = await WebBrowser.openBrowserAsync(uri);
  }

  /**
   * Opens mail client and open new mail with provided information (to, subject)
   */
  _onSendMail() {
    const to = ['mirjamthomet@gmail.com', 'info@bbacksoon.com'] // string or array of email addresses
    email(to, {
      subject: 'Feedback Bspontanious App'
    }).catch(console.error)
  }

  /**
   * Get Typs from Firebase
   */
  getTyps() {
    let typs
    firebase.database().ref('momentTyps').once('value', function (snapshot) {
      typs = snapshot.val();
    }).then(() => {
      const typData = Object.values(typs)
      let imageUrl = typData[this.props.user.typ.number - 1].avatarImage.url
      let name = typData[this.props.user.typ.number - 1].name
      this.setState({ typs: typs, typImage: imageUrl, typText: name })
    }).catch((error) => {
      console.log(error)
    })
  }

  /**
   * Changes image, text of slider depending on the slider value
   * @param {*} typ new typ where slider is
   */
  _onTypChanged(typ) {
    const typData = Object.values(this.state.typs)
    let imageUrl = typData[typ].avatarImage.url
    let name = typData[typ].name
    this.setState({ typImage: imageUrl, typText: name })
  }

  /**
   * Updates username on firebase for loggedin user
   */
  _onUsernameChange() {
    if (this.state.newUsername) {
      let newUsername = {
        username: this.state.newUsername
      }
      var ref = firebase.database().ref('users/' + this.props.user.uid);
      ref.update(newUsername);
      this.props.updateUsernameUser(newUsername.username);
    } else {
      this.setState({ errorMessageUsername: errorHandling('auth/no-username') });
    }
  }

  /**
   * Changes password for user, user has to provide old password and give twice the new.
   * Values get validated before sent to firebase
   */
  _onPasswordChange() {
    //check if the two new passwords match
    if (this.state.newPassword === this.state.newPasswordRepeat) {
      //check if password is minimum 6 chars long
      if (this.state.newPassword.length >= 6) {
        //get current user 
        let user = firebase.auth().currentUser;
        //save old credenital into an object
        let credential = firebase.auth.EmailAuthProvider.credential(
          this.props.user.email,
          this.state.oldPassword
        )

        let newPassword = this.state.newPassword;
        //reauthenticate user before change password
        user.reauthenticateAndRetrieveDataWithCredential(credential).then(() => {
          //update password on firebase
          user.updatePassword(newPassword).then(() => {
            this.setState({ passwordChanged: true });
            this.setState({ errorMessagePassword: "" });
          }).catch((error) => {
            this.setState({ errorMessagePassword: (errorHandling(error.code)) })
            errorMessage = (errorHandling(error.code));
          });
        }).catch((error) => {
          console.log(error.code)
          if (error.code === 'auth/wrong-password') this.setState({ errorMessagePassword: "Altes Passwort ist falsch" })
          else this.setState({ errorMessagePassword: (errorHandling(error.code)) })
        });
      } else {
        this.setState({ errorMessagePassword: (errorHandling('auth/to-short-password')) })
      }
    } else {
      this.setState({ errorMessagePassword: (errorHandling('auth/password-not-match')) })
    }
  }

  /**
   * Saves new typ into users firebase
   */
  changeTyp() {
    if (this.state.typ != this.props.user.typ) {
      let newTyp = {
        typ: this.state.typ
      }
      var ref = firebase.database().ref('users/' + this.props.user.uid);
      ref.update(newTyp);
      this.props.updateTypUser(this.state.typ);
    }
  }

  /**
   * Goes out of settings back to the profile
   */
  goBack() {
    this.setState({
      errorMessagePassword: "",
      errorMessageUsername: "",
    })
    this.props.navigation.navigate('Profile');
  }

  render() {
    if (!this.state.userSettings) {
      /**
       * Show User Settings
       */
      return (
        <Container>
          <View style={stylesSettings.background}>
            <Grid>
              <Row size={20}>
                <Left style={stylesSettings.body}>
                  <Button transparent style={stylesSettings.backbutton} onPress={() => this.goBack()}>
                    <Icon style={stylesSettings.backButtonText} name="arrow-back"></Icon>
                    <Text style={stylesSettings.backButtonText}>Zurück zum Profil</Text>
                  </Button>
                </Left>
                <Right style={stylesSettings.body}>
                  <Icon style={stylesIcons.gearYellow} type="FontAwesome" name="gear"
                    onPress={() => this.props.navigation.navigate('Profile')} />
                </Right>
              </Row>
              <Row size={80}>
                <Body style={stylesSettings.body}>
                  <Button style={stylesButton.buttonSettings} onPress={() => this.setState({ userSettings: true })}>
                    <Text style={stylesButton.buttonText}>Profil bearbeiten</Text>
                  </Button>
                  <Button style={stylesButton.buttonSettings} onPress={() => this._onSendMail()}>
                    <Text style={stylesButton.buttonText}>Mitteilung senden</Text>
                  </Button>
                  <Button style={stylesButton.buttonSettings} onPress={() => this._openLink('https://bbacksoon.com/de-ch/datenschutz/')} marginTop={40}>
                    <Text style={stylesButton.buttonText}>Datenschutz</Text>
                  </Button>
                  <Button style={stylesButton.buttonSettings} onPress={() => this._openLink('https://bbacksoon.com/de-ch/nutzungsbedingungen/')}>
                    <Text style={stylesButton.buttonText}>AGB</Text>
                  </Button>
                  <Button style={stylesButton.buttonSettings} onPress={() => this._openLink('https://bbacksoon.com/de-ch/impressum/')}>
                    <Text style={stylesButton.buttonText}>Impressum</Text>
                  </Button>
                  {/* Logout from App */}
                  <Button style={stylesButton.buttonSettings} onPress={() => this._onLogout()}>
                    <Text style={stylesButton.buttonText}>Ausloggen</Text>
                  </Button>
                </Body>
              </Row>
              <Row>
              </Row>
            </Grid>
            <Text>
            </Text>

          </View>
        </Container>
      );
    }
    if (this.state.userSettings) {
      /**
       * Show Profile Edit Screen
       */
      return (
        <ScrollView>
          <View style={stylesSettings.background}>
            <Left style={stylesSettings.icon}>
              <Button transparent style={stylesSettings.backbutton} onPress={() => this.setState({ userSettings: false })}>
                <Icon style={stylesSettings.backButtonText} name="arrow-back"></Icon>
                <Text style={stylesSettings.backButtonText}>Zurück zu den Einstellungen</Text>
              </Button>
            </Left>

            <Card style={stylesSettings.positionCard}>

              <CardItem style={stylesSettings.content}>
                <Text style={stylesFont.headerProfilEdit}>Profil bearbeiten</Text>
              </CardItem>
              <CardItem style={{ width: '100%' }}>
                <Form style={{ width: '100%' }}>
                  <Item>
                    <Input style={stylesSettings.content} placeholder="Altes Passwort" name="oldPassword"
                      secureTextEntry={true}
                      onChangeText={(password) => this.setState({ oldPassword: password })}
                      returnKeyType={"next"}
                      value={this.state.oldPassword} />
                  </Item>
                  <Item>
                    <Input style={stylesSettings.content} placeholder="Neues Passwort" name="newPassword"
                      secureTextEntry={true}
                      onChangeText={(password) => this.setState({ newPassword: password })}
                      returnKeyType={"next"}
                      value={this.state.newPassword} />
                  </Item>
                  <Item>
                    <Input style={stylesSettings.content} placeholder="Neues Passwort wiederholen" name="newPasswordRepeat"
                      secureTextEntry={true}
                      onChangeText={(password) => this.setState({ newPasswordRepeat: password })}
                      returnKeyType={"next"}
                      value={this.state.newPasswordRepeat} />
                  </Item>
                  <Text>{this.state.errorMessagePassword}</Text>
                  {this.state.passwordChanged &&
                    <View>
                      <Icon style={{ color: 'green' }} name='cloud-check' />
                      <Text>Passwort wurde erfolgreich geändert</Text>
                    </View>
                  }
                  <Button style={stylesButton.buttonYellow} onPress={() => this._onPasswordChange()}>
                    <Text style={stylesButton.buttonText}>Passwort ändern</Text>
                  </Button>
                </Form>
              </CardItem>
              <CardItem style={{ width: '100%' }}>
                <Form style={{ width: '100%' }}>
                  {/* Username Change*/}
                  <Item>
                    <Input style={stylesSettings.content} placeholder="Neuer Benutzername" name="username"
                      onChangeText={(username) => this.setState({ newUsername: username })}
                      returnKeyType={"next"}
                      value={this.state.newUsername} />
                  </Item>
                  <Text>{this.state.errorMessageUsername}</Text>
                  <View style={{ marginBottom: 20 }}>
                    <Button style={stylesButton.buttonYellow} onPress={() => this._onUsernameChange()}>
                      <Text style={stylesButton.buttonText}>Benutzername ändern</Text>
                    </Button>
                  </View>
                </Form>
              </CardItem>
              <CardItem style={stylesSettings.sliderContent}>
                {/* Typ selector */}
                {this.state.typs &&
                  <View>
                    <Image source={{ uri: this.state.typImage }} style={stylesSettings.sliderImage} />
                    <Slider
                      maximumValue={Object.keys(this.state.typs).length - 1}
                      minimumValue={0}
                      value={this.props.user.typ.number - 1}
                      step={1}
                      onValueChange={(typ) => {
                        this.setState({
                          typ: {
                            number: Object.values(this.state.typs)[typ].number,
                            key: Object.keys(this.state.typs)[typ],
                            name: Object.values(this.state.typs)[typ].name,
                            avatarImage: Object.values(this.state.typs)[typ].avatarImage
                          },
                        })
                        this._onTypChanged(typ);
                      }}
                    />
                    <Text style={stylesFont.text}>{this.state.typText}</Text>
                  </View>
                }
              </CardItem>
              <CantonPicker registrationForm={false} />
              <View style={stylesSettings.buttonSave}>
                <Button style={stylesButton.buttonYellow} onPress={() => this.changeTyp()}>
                  <Text style={stylesButton.buttonText}>Speichern</Text>
                </Button>
              </View>
            </Card>

          </View>
        </ScrollView>
      )
    }

  }
}


/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, {
  clearUser, updateTypUser, updateUsernameUser
})(Settings);