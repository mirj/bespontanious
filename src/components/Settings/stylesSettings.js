import { Platform, StyleSheet } from 'react-native';

export default stylesSettings = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#00313D',
        padding: 10,
    },
    content: {
        alignSelf: 'center',
        justifyContent: 'center',
    },
    icon: {
        flex: 1,
        position: 'relative',
        top: 10,
    },
    positionCard: {
        flex: 2,
        position: 'relative',
        top: 20,
        marginBottom: 120,
    },
    body: {
        alignSelf: 'flex-start'
    },
    sliderContent: {
        justifyContent: 'center'
    },
    sliderImage: {
        resizeMode: 'contain',
        height: 200,
        flex: 1,
    },
    backbutton: {
        margin: 0,
        borderWidth: 0,
        padding: 0
    },
    backButtonText: {
        color: '#fff',
        paddingLeft: 0
    },
    cantonPicker: {
        alignSelf: 'center',
        marginTop: 15,
        marginBottom: 15,
        width: '80%', 
    },
    buttonSave: {
        width: '70%',
        marginTop: 15,
        marginBottom: 15,
        alignSelf: 'center',
    },
});