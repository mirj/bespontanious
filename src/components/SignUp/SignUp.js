import React, { Component } from 'react';
import { Container, Button, Text, Spinner, Icon, Content } from 'native-base';
import { connect } from 'react-redux';
import { View, BackHandler, StyleSheet, ImageBackground } from 'react-native';
import RegistrationStep from '../RegistrationStep/RegistrationStep';
import { getKeyByValue } from '../../helpers/HelperFunctions';
import {
  setRegistrationStep,
  updateUIDUser,
  loadingChanged,
  updateErrorRegistration,
  passwordTooShortRegistration,
  inputMissingRegistration,
  clearRegistration,
  registrationComplete,
} from '../../actions';
import errorHandling from '../../constants/errorFunction';
import * as firebase from 'firebase';
import { regSteps } from '../../constants/General';
import stylesButton from '../../constants/Styling/stylesButton';
import stylesSignup from './stylesSignUp';
import EmailValidator from 'email-validator';

/**
 * Component for registration
 */
class SignUp extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      error: "",
      loading: false,
      fbAuthCredential: "",
      disabled: false
    }
  }

  componentWillMount() {
    //Eventlistener for android backbutton
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    //remove eventlistener for android backbutton
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  /**
   * handle android backbutton
   */
  handleBackButtonClick() {
    this.goToPrevStep();
    return true;
  }

  /**
   * Handle reach registration step. Validate inputs and store them on firebase.
   * The last step creates the new user and logs in.
   */
  _handleRegistrationStep() {
    // Validate email and password on first step.
    this.setState({ disabled: true });
    if (this.props.registration.registrationStep === regSteps.emailPassword) {
      if (this.validateEmailPassword()) {
        firebase.auth().fetchSignInMethodsForEmail(this.props.registration.email)
          .then((signInMethods) => {
            if (signInMethods.length > 0) {
              this.props.updateErrorRegistration([errorHandling('auth/user-exists')]);
              this.setState({ disabled: false });
              return false
            } else {
              this.goToNextStep();
              return true
            }
          })
          .catch((error) => {
            this.props.updateErrorRegistration([errorHandling(error.code)]);
            this.setState({ disabled: false });
            return false;
          });
      }
      // Standard input validation on other steps.
    } else {
      isTypStep = (this.props.registration.registrationStep === regSteps.typ)
      fieldName = getKeyByValue(regSteps, this.props.registration.registrationStep)
      if (!isTypStep) {
        isValid = this.validateInput(fieldName)
      } else {
        isValid = true
      }

      if (isValid) {
        // Create user after validating last step.
        if (this.props.registration.registrationStep === Object.getOwnPropertyNames(regSteps).length) {
          this.createUser();
        } else {
          this.goToNextStep()
        }
      }
      this.setState({ disabled: false });
    }
  }

  /**
   * Move the registration form one step forward
   */
  goToNextStep() {
    this.setState({ disabled: false });
    this.props.setRegistrationStep(this.props.registration.registrationStep + 1);
  }

  /**
   * Move the registration form one step forward
   */
  goToPrevStep() {
    if (this.props.registration.registrationStep !== regSteps.emailPassword) {
      this.props.setRegistrationStep(this.props.registration.registrationStep - 1);
    } else {
      this.props.navigation.navigate('Welcome')
    }
  }

  /**
   * Writes the Userdata to Firebase and navigates to App when Successful
   * @param {} uid uid from user in firebase
   * @param {*} registration Userdata from registration forms.
   */
  writeUserData(uid, registration) {
    firebase.database().ref('users/' + uid).set({
      uid: uid,
      username: registration.username,
      email: registration.email,
      birthday: registration.birthday,
      typ: registration.typ,
      canton: registration.canton,
      hasTutorialMoment: false
    }).then(() => {
      this.props.clearRegistration()
      
    }).catch((error) => {
      console.log(error)
    })
  }

  /**
   * Signs User in with Facebook to firebase authentification
   */
  async _onFacebookLogin() {
    // login with facebook using expo
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('319890002076323', { permission: ['public_profile'] })
    // when succesful store credential in state and go to next regstep.
    if (type == 'success') {
      this.setState({ fbAuthCredential: firebase.auth.FacebookAuthProvider.credential(token) })
      this.props.setRegistrationStep(regSteps.username)
    }
  }

  /**
   * Create a new user on firebase using the registration data.
   */
  createUser() {
    this.setState({ loading: true });
    // Login using fb credentials.
    if (this.state.fbAuthCredential) {
      firebase.auth().signInAndRetrieveDataWithCredential(this.state.fbAuthCredential)
        .then((user) => {
          this.props.registration.email = user.user.email
          this.setState({ loading: false });
          this.props.updateUIDUser(user.user.uid);
          // Save Userdata to Firebase
          this.writeUserData(this.props.user.uid, this.props.registration);
          this.props.clearRegistration()
        })
        .catch(error => {
          /*set loading to false */
          this.setState({ loading: false });
          this.props.updateErrorRegistration([errorHandling(error.code)]);
        })
    // Login using email and password.
    } else {
      firebase.auth().createUserWithEmailAndPassword(this.props.registration.email, this.props.registration.password)
        .then((user) => {
          // Save Userdata to Firebase
          this.setState({ loading: false });
          this.props.updateUIDUser(user.user.uid);
          this.writeUserData(user.user.uid, this.props.registration);
        })
        .catch(error => {
          console.log(error)
          this.setState({ loading: false });
          this.props.updateErrorRegistration([errorHandling(error.code)]);
        });
    }

  }

  /**
   * Check if the given feild is empty.
   * @param {String} field Name of feild to validate
   * @return {Boolean} If the feild has input
   */
  validateInput(field) {
    this.props.inputMissingRegistration(field, false);
    this.props.updateErrorRegistration("");
    if (!this.props.registration[field]) {
      this.props.inputMissingRegistration(field, true);
      this.props.updateErrorRegistration([errorHandling('auth/no-' + field)]);
      return false;
    }

    return true;
  }

  /**
   * Validate both email and password feilds.
   * Standard input validation and check if password is at least 6 characters.
   * @return {Boolean} If password and email are valid
   */
  validateEmailPassword() {
    this.props.passwordTooShortRegistration(false);
    let isValid = true;
    isValid = this.validateInput('email')
    isValid = this.validateInput('password')
    let errors = []

    //No Email
    if (!this.props.registration.email) {
      isValid = false;
      errors.push(errorHandling('auth/no-email'));
    }

    // Check email syntax
    if (this.props.registration.email && !EmailValidator.validate(this.props.registration.email)) {
      isValid = false;
      errors.push(errorHandling('auth/invalid-email'));
    }

    // No Password
    if (!this.props.registration.password) {
      isValid = false;
      errors.push(errorHandling('auth/no-password'))
    }

    // Password must be at least 6 chars
    if (this.props.registration.password && this.props.registration.password.length < 6) {
      isValid = false;
      this.props.passwordTooShortRegistration(true);
      errors.push(errorHandling('auth/to-short-password'))
      this.setState({ loading: false });
      console.log(this.state.error)
    }
    if (errors) this.props.updateErrorRegistration(errors)
    this.setState({ disabled: false });
    return isValid;
  }

  render() {
    return (
      <Container style={stylesSignup.container}>
        <ImageBackground source={require('../../../assets/images/Registrieren.jpg')} style={stylesSignup.backgroundImage}>
          <Button style={stylesButton.backButton} marginTop={20} onPress={() => this.goToPrevStep()}>
            <Icon name="arrow-back"></Icon>
            <Text style={stylesButton.backButtonText}>Zurück</Text>
          </Button>
          <Content>
            <RegistrationStep />
            <Button 
                style={[stylesButton.buttonWhite, stylesSignup.button]}
                disabled={this.state.disabled} 
                onPress={() => this._handleRegistrationStep()}
                onSubmitEditing={() => this._handleRegistrationStep()}
              >
              <Text style={stylesButton.buttonText}>Weiter</Text>
            </Button>
            {/*Only show Facebook registration in first registration step*/}
            {this.props.registration.registrationStep === regSteps.emailPassword &&
              <Button iconLeft style={stylesButton.buttonFacebook} onPress={() => this._onFacebookLogin()}>
                <Icon style={stylesButton.buttonFacebookIcon} type="EvilIcons" name="sc-facebook" />
                <Text style={stylesButton.buttonFacebookText}>Mit Facebook registrieren</Text>
              </Button>}
            {this.state.loading &&
              <View style={stylesSignup.loading}>
                <Spinner color='#FCEC45' />
              </View>
            }
          </Content>
        </ImageBackground>
      </Container >
    )
  }
}


const mapStatetoProps = state => {
  return {
    registration: state.registration,
    user: state.user
  };
};

export default connect(mapStatetoProps, {
  setRegistrationStep,
  updateUIDUser,
  loadingChanged,
  updateErrorRegistration,
  passwordTooShortRegistration,
  inputMissingRegistration,
  clearRegistration,
  registrationComplete,
})(SignUp);