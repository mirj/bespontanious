import { StyleSheet } from 'react-native';

export default stylesSignup = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        padding: 15,
        width: '100%',
    },
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 15,
    },
    loading: {
        position: 'absolute',
        backgroundColor: '#F5FCFF88',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    content: {
        padding: 15
    },
    button: {
        marginBottom: 20
    },
    header: {
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 10,
        height: 45,
    },
});