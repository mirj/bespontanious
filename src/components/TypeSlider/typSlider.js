import React, { Component } from 'react';
import { View, Slider, Image } from 'react-native';
import { Text } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import stylesRegistrationStep from '../RegistrationStep/stylesRegistrationStep';
import {
    updateTypRegistration,
    setRegistrationStep,
} from '../../actions';

/**
 * Component for typslider
 */
class TypSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            typs: [],
            selectedTyp: 0,
            isLoading: true
        }
    }

    componentDidMount() {
        // Get data from firebase
        this.getTyps();
    }

    /**
     * when typ changed update component state
     * @param {Object} typ 
     */
    _onTypChanged(typ) {
        this.setState({ selectedTyp: typ })
    }

    /**
     * get all the typs from firebase
     */
    getTyps() {
        let typs
        firebase.database().ref('momentTyps').once('value', function (snapshot) {
            typs = snapshot.val();
        }).then(() => {
            this.setState({ typs: typs, isLoading: false })
            const typData =  Object.values(typs)[0]
            let typKey = Object.keys(typs)[0]
            this.props.updateTypRegistration({
                number: typData.number,
                key: typKey,
                name: typData.name,
                avatarImage: typData.avatarImage
            });
        }).catch((error) => {
            console.log(error)
        })
    }

    render() {
        let typKey
        let typs = Object.entries(this.state.typs)
        const typData =  Object.values(this.state.typs)
        let typObject = typs.find(key => key[1].number === this.state.selectedTyp + 1)
        if (typObject) typKey = typObject[0]
        return (
            <View>
                {(typData[this.state.selectedTyp]) &&
                    <View>
                        <Image
                            style={stylesRegistrationStep.imageSpontiDna}
                            source={{ uri: typData[this.state.selectedTyp].avatarImage.url }}
                        />
                        <Slider
                            maximumValue={typData.length - 1}
                            minimumValue={0}
                            step={1}
                            thumbTintColor={'#FCEC45'}
                            minimumTrackTintColor={'#FCEC45'}
                            maximumTrackTintColor={'#A2A79E'}
                            onSubmitEditing={() => this.props.setRegistrationStep(regSteps.canton)}
                            onValueChange={(typ) => {
                                this._onTypChanged(typ);
                                this.props.updateTypRegistration({
                                    number: typData[typ].number,
                                    key: typKey,
                                    name: typData[typ].name,
                                    avatarImage: typData[typ].avatarImage
                                });
                            }}
                        />

                        <View>
                            <Text style={stylesRegistrationStep.textSpontiDna}>
                                {Object.values(this.state.typs)[this.state.selectedTyp].name}
                            </Text>
                            <Text style={stylesRegistrationStep.descriptionSpontiDna}>
                                {Object.values(this.state.typs)[this.state.selectedTyp].description}
                            </Text>
                        </View>
                        
                    </View>
                }
            </View>
        );
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        user: state.user
    }
}

/*Connects Component to Redux Store */
export default connect(mapStateToProps, {
    updateTypRegistration,
    setRegistrationStep
})(TypSlider);