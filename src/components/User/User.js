import React, { Component } from 'react';
import { Container, Text, Right, Icon, Body, Grid, Row, View } from 'native-base';
import { connect } from 'react-redux';
import { Image } from 'react-native'
import {
  updateUIDUser,
  updateEmailUser,
  updateUsernameUser,
  updateBirthdayUser,
  updateTypUser
} from '../../actions';
import ChallengePoints from '../Points/Points'
import Awards from '../Awards/Awards'
import stylesUser from './stylesUser';
import stylesIcons from '../../constants/Styling/stylesIcons';
import { showMessage } from "react-native-flash-message";

/**
 * Component for user profile
 */
class User extends Component {
  constructor(props) {
    super(props);
  }

  openSettings() {
    if (!this.props.isConnected) {
      showMessage({
        message: "Keine verbindung",
        description: "Diese Funktion ist offline nicht verfügbar",
        type: "default",
        backgroundColor: "#FCEC45", // background color
        color: "#707070"
      });
    } else {
      this.props.navigation.navigate('UserSettings')
    }
  }

  render() {
    return (
      <Container>
        <View style={stylesUser.background}>
          <Grid>
            <Row size={10}>
              <Right style={stylesSettings.body}>
                <Icon style={stylesIcons.gearWhite} type="FontAwesome" name="gear"
                  onPress={() => this.openSettings()} />
              </Right>
            </Row>
            <Image source={{ uri: this.props.user.typ.avatarImage.url }} style={stylesUser.image} />
            <Row Row size={80}>
              <Body style={stylesSettings.body}>
                <Text style={stylesUser.username}>{this.props.user.username}</Text>
                <Text style={stylesUser.typ}>{this.props.user.typ.name.toUpperCase()}</Text>
                <ChallengePoints />
                <Awards />
              </Body>
            </Row>
          </Grid>
        </View>

      </Container>
    );
  }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
  return {
    user: state.user,
    isConnected: state.network.isConnected
  }
}

export default connect(mapStateToProps, {
  updateEmailUser,
  updateUsernameUser,
  updateBirthdayUser,
  updateTypUser,
  updateUIDUser
})(User);