import {StyleSheet} from 'react-native';
/*import { SSL_OP_TLS_BLOCK_PADDING_BUG } from 'constants';*/

export default stylesUser = StyleSheet.create({
    background:{
        position: 'absolute',
        backgroundColor: '#00313D',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,  
    },
    username:{
        color: '#fff',
        fontSize: 30,
        fontWeight: '200',
        marginTop: 20,
        fontFamily: 'Robot_light',
    },
    typ: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 10
    },
    icon: {
        color: '#FCEC45',
        fontSize: 25,
    },
    image: {
        resizeMode: 'contain',
        height: 150
    },
    text:{
        color: '#fff',
        fontSize: 16,
        fontWeight: 'normal',
        marginLeft: 10
    },
    textAward:{
        color: '#fff',
        fontSize: 16,
        fontWeight: 'normal',
        textAlign: 'center'
    },
    iconAward: {
        color: "#FCEC45", 
        marginTop: 10
    },
    containerAward: {
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 30,
        paddingLeft: '2%',
        paddingRight: '2%'
    },
    iconTextContainer: {
        flexDirection: 'row', 
        paddingTop: 10,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    awardContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        flexBasis: '50%',
        flexGrow: 1,
    }
});