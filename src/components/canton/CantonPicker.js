import React, { Component } from 'react';
import { View, } from 'react-native';
import { Form, Item, Picker } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { Icon } from 'react-native-elements';
import {
    updateCantonRegistration,
    setRegistrationStep,
    updateCantonUser
} from '../../actions';
import stylesSettings from '../Settings/stylesSettings';

class CantonPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cantons: [],
            selectedValue: this.props.user.canton.name
        }
    }

    componentDidMount() {
        this.getCantons();
    }

    /**
     * Gets cantons from firebase database
     */
    getCantons() {
        let cantons
        firebase.database().ref('momentCantons').once('value', function (snapshot) {
            cantons = snapshot.val();
        }).then(() => {
            this.setState({ cantons: cantons })
            // Set initial state of reg form
            if (this.props.registrationForm) {
                let canton = Object.values(cantons)[0]
                let cantonKey = Object.keys(cantons)[0]
                let initialCanton = { key: cantonKey, name: canton.title };
                this.props.updateCantonRegistration(initialCanton)
            }
        }).catch((error) => {
            console.log(error)
        })
    }

    /**
     * saves selected canton into redux and firebase
     * @param {*String} cantonName selected canton title
     */
    _onSelect(cantonName) {
        let cantons = Object.entries(this.state.cantons)
        let canton = cantons.find(key => key[1].title === cantonName)
        let newCanton = { key: canton[0], name: cantonName };
        if (this.props.registrationForm) {
            this.props.updateCantonRegistration(newCanton)
        } else {
            if (canton != this.props.user.canton.name) {
                let newCantonObject = {
                    canton: newCanton
                }
                var ref = firebase.database().ref('users/' + this.props.user.uid);
                ref.update(newCantonObject);
                this.props.updateCantonUser(newCanton);
            }
        }

        this.setState({ selectedValue: cantonName })
    }

    render() {
        const cantons = Object.keys(this.state.cantons).map((cantonKey, i) => {
            let canton = this.state.cantons[cantonKey]
            return (
                <Picker.Item key={cantonKey} label={canton.title} value={canton.title} />
            )
        })
        return (
            <View>
                <Form style={stylesSettings.cantonPicker}>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="keyboard-arrow-down" />}
                            style={{ width: '90%' }}
                            placeholder="Select your SIM"
                            placeholderStyle={{ color: "#707070" }}
                            placeholderIconColor="#707070"
                            selectedValue={this.state.selectedValue}
                            onValueChange={(canton) => this._onSelect(canton)}
                        >
                            {cantons}
                        </Picker>
                    </Item>
                </Form>
            </View>
        );
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        user: state.user,
        registration: state.registration
    }
}

/*Connects Component to Redux Store */
export default connect(mapStateToProps, {
    updateCantonRegistration,
    setRegistrationStep,
    updateCantonUser
})(CantonPicker);