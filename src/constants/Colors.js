const tintColor = '#00313D';

export default {
  tintColor,
  tabIconDefault: '#707070',
  tabIconSelected: '#0E7189',
  tabBar: '#fefefe',
};
