export const regSteps = {
    "emailPassword": 1,
    "username": 2,
    "birthday": 3,
    "typ": 4,
    "canton": 5,
}

export const regStepTitles = {
    1: "Registrieren",
    2: "Wie möchtest du heissen?",
    3: "Wann ist dein Geburtstag?",
    4: "Was ist deine spontanitäts DNA?",
    5: "Wo wohnst du?"
}