import {StyleSheet} from 'react-native';


export default stylesBackground = StyleSheet.create({
backgroundColor:{
    position: 'absolute',
    backgroundColor: '#00313D',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,  
},
});