import {StyleSheet} from 'react-native';


export default stylesButton = StyleSheet.create({
buttonWhite:{
    width: '100%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderRadius: 10,
},
buttonText:{
    textAlign: 'center',
    fontSize: 16,
    color: '#707070',
},
backButtonText:{
    textAlign: 'left',
    paddingLeft: 0,
},
backButton: {
    backgroundColor: 'transparent',
    elevation: 0,
    margin: 0
},
buttonTextCard:{
    fontSize: 16,
    color: '#707070',
    paddingTop: 8,
},
buttonFacebook:{
    width: '100%',
    backgroundColor: '#475993',
    borderRadius: 10,
},
buttonFacebookIcon: {
    fontSize: 38,
    color: '#ffffff',
},
buttonFacebookText: {
    flexGrow: 1,
    textAlign: 'center',
    paddingLeft: 0
},
buttonSettings:{
    width: '100%',
    justifyContent: 'center',
    backgroundColor: '#fff',
    marginTop: 10,
},
buttonYellow:{
    width: '100%',
    backgroundColor: '#FCEC45',
    justifyContent: 'center',
    borderRadius: 10,
},
buttonOffline:{
    width: '100%',
    backgroundColor: '#E0E0E0',
    justifyContent: 'center',
    borderRadius: 10,
},
buttonMargin: {
    marginTop: 40,
},
buttonWhiteMomente:{
    width: '80%',
    backgroundColor: '#EBEBEB',
    justifyContent: 'center',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 0,
    marginLeft: 0,
    alignItems: 'stretch',
    marginLeft: '20%',
},
buttonGrayMomente:{
    width: '80%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 0,
    alignItems: 'stretch',
    marginRight: '20%',
},
});