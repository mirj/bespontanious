import {Platform, StyleSheet} from 'react-native';

export default stylesCard = StyleSheet.create({
    card:{
        borderRadius: 5,
        overflow: 'hidden',
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        width: '100%',
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 4,
            },
        shadowOpacity: 0.32,
        shadowRadius: 5.5,
        elevation: 9,
        zIndex: 200,
    },
    content:{
        width: '90%',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginBottom: 40,
        padding: 0,
        zIndex: 200,
    },
    contentStep: {
        width: '90%',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        padding: 0,
        marginBottom: 0,
        ...Platform.select({
            ios: {
                zIndex: 2
            },
        })
    },
    screenBackground: {
        backgroundColor: '#00313D',
    },
    scrollView: {
        backgroundColor: '#00313D',
        paddingTop: 40,
    },
    image:{
        height: 200,
        width: null,
        flex: 1,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    contentButton:{
        width: '70%',
        top: '-4%',
        zIndex: -2,
    },
    contentButtonMomente:{
        position: 'relative',
        width: '70%',
        top: -10,
        zIndex: -400
    },
    contentButtonMomenteSteps:{
        alignSelf: 'flex-start',
        position: 'relative',
        width: '70%',
        top: -10,
        zIndex: -400
    },
});