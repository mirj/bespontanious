import {StyleSheet} from 'react-native';


export default stylesErrorMessage = StyleSheet.create({
    errorMessageYellow: {
        color: '#FCEC45',
        textAlign: 'center',
        paddingTop: 10,
    },
    errorMessage: {
        color: 'red',
        textAlign: 'center',
        paddingTop: 10,
    },
});