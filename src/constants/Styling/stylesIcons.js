import {StyleSheet} from 'react-native';


export default stylesIcons = StyleSheet.create({
    gearWhite:{
        color: '#fff',
        marginRight: 15,
        marginTop: 15
    },
    gearYellow:{
        color: '#FCEC45',
        marginRight: 5,
        marginTop: 5
    },
    arrowLeft:{
        color: '#fff',
        marginTop: 5
    },
});