import {StyleSheet} from 'react-native';


export default stylesOverlay = StyleSheet.create({
    containerOverlay:{
        flex: 1,
    },
    contentOverlay:{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        borderRadius: 5,
    },
    imageOverlay:{
        width: 200,
        height: 200,
        marginTop: 90,
    },
    textOverlay:{
        fontFamily: 'Roboto',
        textAlign: 'center',
        fontSize: 30,
        fontWeight: '100',
        color: '#707070',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 70,
    },
    iconOverlay:{
        position: 'absolute',
        right: 20,
        top: 10,
        backgroundColor: null,
    }
});