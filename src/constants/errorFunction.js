export default errorHandling = (errorCode) => {
    switch(errorCode) {
        case 'auth/invalid-email': return 'E-Mailadresse falsch formatiert';
        case 'auth/user-not-found': return 'Falsche Email oder Passwort';
        case 'auth/wrong-password': return 'Falsche Email oder Passwort';
        case 'auth/user-exists': return 'E-Mailadresse bereits verwendet';
        case 'auth/no-required': return 'Bitte fülle alle Felder aus';
        case 'auth/to-short-password': return 'Passwort muss mindestens 6 Zeichen haben'
        case 'auth/no-username': return 'Bitte füge einen Usernamen ein'
        case 'auth/no-email': return 'Bitte füge eine Email ein'
        case 'auth/no-password': return 'Bitte füge ein Passwort ein'
        case 'auth/no-birthday': return 'Bitte füge einen Geburtstag ein'
        case 'auth/no-canton': return 'Bitte füge einen Kanton ein'
        case 'auth/no-typ': return 'Bitte füge eine Spontanitats-DNA ein'
        case 'auth/password-not-match': return 'Die Passwörter stimmen nicht überein'
        default: return 'Ein Fehler ist aufgetreten';
    }
};