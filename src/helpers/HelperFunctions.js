import * as firebase from 'firebase';
import {
    updateUIDUser,
    updateEmailUser,
    updateUsernameUser,
    updateBirthdayUser,
    updateAbosUser,
    updateTypUser,
    updateMomentsUser
  } from '../actions';
import { Permissions, Notifications } from 'expo';

/**
 * General helper functions
 */

/**
 * Returns a the key from the given object with the given value
 * @param {} object Object containing the key your are looking for
 * @param {*} value 
 */
export function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
};

/**
 * Request permission for Push Notification and return token.
 */
async function registerForPushNotificationsAsync(userUid) {
    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        return;
    }
    // Get the token that uniquely identifies this device
    //let token = await Notifications.getExpoPushTokenAsync();
    Notifications.getExpoPushTokenAsync().then((token) => {
        // POST the token to your backend server from where you can retrieve it to send push notifications.
        firebase.database().ref('users/' + userUid).update({
            token
        }, function (error) {
            if (error) {
                console.log(error)
            }
        });
    }).catch((error) => console.log(error))
}

module.exports.registerForPushNotificationsAsync = registerForPushNotificationsAsync;