import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import WelcomeScreen from '../screens/WelcomeScreen';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignupScreen';
import ProfileScreen from '../screens/ProfileScreen';
import AboScreen from '../screens/AboScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import MainTabNavigator from './MainTabNavigator';
import SettingsScreen from '../screens/SettingsScreen';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';

/**
 * Appmainnavigation
 */

 /*Profile navigation stack*/
const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
  UserSettings: SettingsScreen,
});

/*Abo navigation stack*/
const AboStack = createStackNavigator({
  Abo: AboScreen
});

/*App navigation stack*/
const AppStack = createStackNavigator({
  Main: MainTabNavigator,
  Profile: ProfileStack,
  Abo: AboStack,
},
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
 })

AppStack.navigationOptions = {
  headerMode: "auto",
}
  

{/* Navigation all the routes */}
export default createSwitchNavigator({
  Loading: AuthLoadingScreen,
  Welcome: WelcomeScreen,
  App: AppStack,
  Login: LoginScreen,
  SignUp: SignUpScreen,
  ForgotPassword: ForgotPasswordScreen,
});