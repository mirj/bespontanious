import React from 'react';
import { Platform, View } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import NewsScreen from '../screens/NewsScreen';
import MomentsScreen from '../screens/MomentsScreen';
import GeneratorScreen from '../screens/GeneratorScreen';
import Notfication from './Notification';

/**
 * Tabnavigation for moments, generator and news. 
 */

 /*Moment tab*/
const MomentsStack = createStackNavigator({
  Moments: MomentsScreen,
});

MomentsStack.navigationOptions = {
  tabBarLabel: 'Momente',
  tabBarOptions: {
    activeTintColor: '#006E89'
  },
  tabBarIcon: ({ focused }) => (
    <View>
      <TabBarIcon
        focused={focused}
        name={Platform.OS === 'ios' ? `ios-text${focused ? '' : '-outline'}` : 'md-text'}
      />
      {/*Notification counter*/}
      <Notfication />
    </View>
  ),
};

 /*News tab*/
const NewsStack = createStackNavigator({
  News: NewsScreen,
});

NewsStack.navigationOptions = {
  tabBarLabel: 'News',
  tabBarOptions: {
    activeTintColor: '#006E89'
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-paper${focused ? '' : '-outline'}`
          : 'md-paper'
      }
    />
  ),
};

 /*Generator tab*/
const GeneratorStack = createStackNavigator({
  Generator: GeneratorScreen,
});

GeneratorStack.navigationOptions = {
  tabBarLabel: 'Zufallsgenerator',
  tabBarOptions: {
    activeTintColor: '#006E89'
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-shuffle${focused ? '' : '-outline'}` : 'md-shuffle'}
    />
  ),
};

export default createBottomTabNavigator({
  Moment: MomentsStack,
  Generator: GeneratorStack,
  News: NewsStack,
});
