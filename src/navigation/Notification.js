import React from 'react';
import { Text, View } from 'native-base';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { updateNotification } from '../actions/index';
import stylesNotification from './stylesNotification';

/**
 * Component for notification counter when new moments
 */

class Notification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            notificationCount: 0
        }
    }

    /**
     * Fetches the Notification Data when component is loaded.
     */
    componentDidMount() {
        // Get data from firebase
        this.getNotification();
    }
    /**
     * Gets the Notification from Firebase in Realtime
     */
    getNotification() {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                let notificationRef = firebase.database().ref('users/' + user.uid + '/moments').orderByChild('/seen').equalTo(false)
                notificationRef.on('value', (snapshot) => {
                    this.props.updateNotification(snapshot.numChildren())
                    this.setState({ notificationCount: snapshot.numChildren() })
                });
            }
        })
    }

    render() {
        if (this.state.notificationCount > 0) {
            return (
                <View style={stylesNotification.badger}>
                    <Text style={stylesNotification.text}>{this.state.notificationCount}</Text>
                </View>
            );
        } else {
            return null
        }
    }
}

/**
 * Connect Component to Redux Store
 * @param {*} state Redux state
 */
function mapStateToProps(state) {
    return {
        notification: state.user,
    }
}
export default connect(mapStateToProps, {
    updateNotification
})(Notification);