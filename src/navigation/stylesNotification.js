import {StyleSheet} from 'react-native';

export default stylesNotification = StyleSheet.create({
    badger: { 
        position: 'absolute', 
        left: 15, 
        top: 0, 
        backgroundColor: 'red', 
        borderRadius: 9, 
        width: 15, 
        height: 15, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    text: {
        color: 'white',
        fontSize: 12
    }
});