import { UPDATE_ABOS, UPDATE_ABO_SLIDE } from '../actions/types';

/* Declare initial state */
const INITIAL_STATE = {
  abos: []
};

/* Reducer for adding the userid */
const aboReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_ABOS:
      return { ...state, abos: action.payload };
    case UPDATE_ABO_SLIDE:
      let updateState = state
      updateState.abos[action.payload.uid].slide = action.payload.slide
      return { ...state, updateState };
    default:
      return state;
  }

}
export default aboReducer