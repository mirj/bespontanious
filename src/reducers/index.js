import { combineReducers } from 'redux';
import UserReducer from './userReducer';
import RegistrationReducer from './registrationReducer';
import aboReducer from './aboReducer';
import newsReducer from './newsReducer';
import { reducer as network } from 'react-native-offline';

{/* Combines all reducers for the store */}
export default combineReducers ({
    user: UserReducer,
    registration: RegistrationReducer,
    abos: aboReducer,
    news: newsReducer,
    network
});