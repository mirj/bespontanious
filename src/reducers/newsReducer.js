import { UPDATE_NEWS } from '../actions/types';

/* Declare initial state */
const INITIAL_STATE = {
  news: []
};

/* Reducer for adding the userid */
const newsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_NEWS:
      return { ...state, news: action.payload };
    default:
      return state;
  }

}
export default newsReducer