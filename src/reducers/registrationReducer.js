import {UPDATE_EMAIL_REGISTRATION, 
        UPDATE_PASSWORD_REGISTRATION, 
        UPDATE_STEP_REGISTRATION, 
        UPDATE_USERNAME_REGISTRATION, 
        UPDATE_BIRTHDAY_REGISTRATION,
        UPDATE_TYP_REGISTRATION,
        UPDATE_ERROR_REGISTRATION, 
        EMAIL_MISSING_REGISTRATION,
        PASSWORD_MISSING_REGISTRATION,
        USERNAME_MISSING_REGISTRATION,
        BIRTHDAY_MISSING_REGISTRATION,
        PASSWORD_TOO_SHORT_REGISTRATION,
        UPDATE_CANTON_REGISTRATION,
        CANTON_MISSING_REGISTRATION,
        INPUT_MISSING_REGISTRATION,
        CLEAR_REGISTRATION
    } from '../actions/types';

/* Declare initial state */
const INITIAL_STATE = {
    email: '', 
    password: '', 
    registrationStep: 1, 
    username: '', 
    typ: 0,
    error: "",
    canton: "",
};

/* Reducer for adding the registration status */
const registrationReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case UPDATE_CANTON_REGISTRATION:
            return{...state, canton: action.payload};
        case UPDATE_EMAIL_REGISTRATION:
            return{...state, email: action.payload};
        case UPDATE_PASSWORD_REGISTRATION:
            return{...state, password: action.payload};
        case UPDATE_STEP_REGISTRATION:
            return{...state, registrationStep: action.payload};
        case UPDATE_USERNAME_REGISTRATION:
            return{...state, username: action.payload};
        case UPDATE_BIRTHDAY_REGISTRATION:
            return{...state, birthday: action.payload};
        case UPDATE_TYP_REGISTRATION:
            return{...state, typ: action.payload};
        case UPDATE_ERROR_REGISTRATION:
        let payload = action.payload
        if (!action.payload.constructor === Array) payload = [payload]
            return{...state, error: payload};
        case INPUT_MISSING_REGISTRATION:
            updateState = {...state};
            updateState[action.field + "Missing"] = action.payload
            return updateState;
        case PASSWORD_TOO_SHORT_REGISTRATION:
            return{...state, passwordShort: action.payload};
        case CLEAR_REGISTRATION:
            return{...state, ...INITIAL_STATE};
        default:
            return state;
    }
}
  export default registrationReducer