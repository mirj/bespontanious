import {
  UPDATE_UID_USER,
  UPDATE_EMAIL_USER,
  UPDATE_USERNAME_USER,
  UPDATE_BIRTHDAY_USER,
  UPDATE_TYP_USER,
  UPDATE_ABOS_USER,
  UPDATE_USERTOKEN,
  UPDATE_MOMENTS_USER,
  UPDATE_MOMENT_STATE,
  CLEAR_USER,
  UPDATE_USER_POINTS,
  UPDATE_CANTON_USER,
  FAKE_CONNECTION_CHANGE,
  UPDATE_USER_NOTIFICATION,
  UPDATE_SEENMODAL_USER,
  UPDATE_USER_TUTORIAL,
  UPDATE_USER_AWARDS,
  UPDATE_USER_REGISTRATION_COMPLETE,
  MOMENTS_SYNCHED
} from '../actions/types';
import { offlineActionTypes } from 'react-native-offline';
import { store } from '../../App';
import * as firebase from 'firebase';

/* Declare initial state */
const INITIAL_STATE = {
  uid: '',
  email: '',
  userName: '',
  birthday: '',
  typ: '',
  canton: '',
  abos: [],
  token: '',
  moments: [],
  points: 0,
  isConn: true,
  notification: 0,
  hasTutorialMoment: false,
  regComplete: false,
  momentsSynchronised: true
};

/* Reducer for adding the userid */
const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_UID_USER:
      return { ...state, uid: action.payload };
    case UPDATE_EMAIL_USER:
      return { ...state, email: action.payload };
    case UPDATE_USERNAME_USER:
      return { ...state, username: action.payload };
    case UPDATE_BIRTHDAY_USER:
      return { ...state, birthday: action.payload };
    case UPDATE_TYP_USER:
      return { ...state, typ: action.payload };
    case UPDATE_ABOS_USER:
      return { ...state, abos: action.payload };  
    case UPDATE_USERTOKEN:
      return { ...state, token: action.payload }; 
    case UPDATE_MOMENTS_USER:
      return { ...state, moments: action.payload };
    case UPDATE_USER_TUTORIAL:
      return { ...state, hasTutorialMoment: action.payload };
    case UPDATE_CANTON_USER:
      return { ...state, canton: action.payload };
    case UPDATE_USER_NOTIFICATION:
      let currentNotification = state.notification
      return { ...state, notification: action.payload };
    case UPDATE_SEENMODAL_USER:
      return { ...state, seenModal: action.payload };  
    case UPDATE_USER_POINTS:
      return { ...state, points: action.payload };
    case UPDATE_USER_REGISTRATION_COMPLETE:
      return { ...state, regComplete: action.payload };
    case UPDATE_USER_AWARDS:
      let newState = state
      newState.newAwards = []

      // Check if there are new awards
      if (state.awards && action.payload) {
        var newAwards = action.payload.filter((award) => {
          let exists = state.awards.some(function (el){
            return el.title === award.title;
          })
          return !exists
        })

        newState.newAwards = newAwards
      }
      newState.awards = action.payload
      return { ...state, ...newState };
    case UPDATE_MOMENT_STATE:
      let updateState = state
      updateState.moments[action.payload.uid].status = action.payload.status
      return { ...state, updateState };
    case CLEAR_USER:
      return { ...state, ...INITIAL_STATE };
    case MOMENTS_SYNCHED:
      return { ...state, momentsSynchronised: true };  
    case offlineActionTypes.CONNECTION_CHANGE:
      let currentState = state
      if (action.payload) {
        // Back Online, synchronise moments, sets momentsSynchronised to true
        synchronizeMoments(currentState.uid, currentState.moments)
      } else {
        // No more connection, set momentsSynchronised to false
        return { ...state, momentsSynchronised: false };
      }      
      return { ...state, isConn: action.payload };
    default:
      return state;
  }

}

/**
 * Update users moments on firebase with given moments
 * @param {Object} moments 
 */
function updateFirebaseMoments(userUid, moments) {
  firebase.database().ref('users/' + userUid).update({
    moments
  }, function (error) {
    if (error) {
      console.log(error)
    }
  });
}

/**
 * Compare the moment with the given key to the liveMoment. Update moments based on differences.
 * This is used to synchronise statuses of moments after a user has been offline.
 * @param {} momentKey 
 * @param {*} liveMoments 
 */
function compareMoment(momentKey, liveMoments, localMoments) {
  let liveMoment = liveMoments[momentKey]
  // Check if moment exists locally
  if (localMoments.hasOwnProperty(momentKey)) {
    localMoment = localMoments[momentKey]
    // Do nothing if status is the same.
    if (localMoment.status === liveMoment.status) return liveMoments
    // If completed on live do nothing
    if (liveMoment.status === 3) return liveMoments
    // If completed locally, Update live moment
    if (localMoment.status === 3) {
      liveMoments[momentKey].status = 3
    }
    // If rejected locally and live moment has default status. Update live moment.
    if (localMoment.status === 1 && liveMoment.status === 0) {
      liveMoments[momentKey].status = 1
    }
  }
  return liveMoments
}

/**
 * Synchronize local and live moments after the user has been offline.
 */
function synchronizeMoments(userUid, localMoments) {
  let liveMoments;
  let path = 'users/' + userUid + '/moments'
  firebase.database().ref(path).once('value', function (snapshot) {
    liveMoments = snapshot.val();
  }).then(() => {
    for (let momentKey of Object.keys(liveMoments)) {
      liveMoments = compareMoment(momentKey, liveMoments, localMoments)
    }
    updateFirebaseMoments(userUid, liveMoments)
    store.dispatch({type: UPDATE_MOMENTS_USER, payload: liveMoments})
    store.dispatch({type: MOMENTS_SYNCHED, payload: true})
  })
}

export default userReducer