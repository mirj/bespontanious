import React from 'react';
import { Content} from 'native-base';
import Abos from '../components/Abos/Abos';
import stylesCard from '../constants/Styling/stylesCard';
import AppHeaderIconsLeft from '../components/AppHeaderLeftIcons/AppHeaderLeftIcons';
import AppHeaderIcons from '../components/AppHeaderIcons/AppHeaderIcons'

/**
 * Screen showing the abos
 */
export default class AboScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = ({navigation}) => {
        return {
            headerRight: (
                <AppHeaderIcons navigation={navigation}/>
            ),
            headerLeft: (
                <AppHeaderIconsLeft navigation={navigation}/>
            ),
            }
    };

  render() {
    return (
      <Content style={stylesCard.screenBackground}>
          <Abos />
      </Content>
    );
  }
}