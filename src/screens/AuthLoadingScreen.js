import React from 'react';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import { registerForPushNotificationsAsync } from '../helpers/HelperFunctions';
import LoadingView from '../components/LoadingView/LoadingView';
import {
  updateUserToken,
  updateConnectivity,
  updateCantonUser,
  updateEmailUser,
  updateUsernameUser,
  updateBirthdayUser,
  updateTypUser,
  updateAbosUser,
  updateUIDUser,
  updateMomentsUser,
  updateUserPoints,
  updateAbos,
  updateNews,
  updateUserTutorial,
  updateUserAwards,
  updateUserFeaturesUsed
} from '../actions';

/*const to note if user recived already tutorial*/
const tutorialFetched = false

/**
 * Loading screen which checks if user is logged in and gets all needed data
 */
class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
   * Function gets loaded when Component starts.
   * Check if user is logged in, gets mobile token
   */
  componentDidMount() {
    if (this.props.isConnected) {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          this._fetchUserData(user.uid)
          this._fetchAboData()
          this._fetchNewsData()

          this.props.navigation.navigate('Generator', { notificationCount: this.props.user.notification });
        } else {
          this.props.navigation.navigate('Welcome');
        }
      });
    } else if (this.props.user) {
      this.props.navigation.navigate('Moments');
    } else {
      Alert.alert(
        'Du bist Offline',
        'Wir haben keine Benutzerdaten auf deinem lokalen Speicher gefunden. Um dich einzuloggen oder zu registrieren brauchst du eine Internet Verbindung',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false }
      )
      this.props.navigation.navigate('Welcome');
    }
  }

  /**
   * Get user data from firebase. Take userId from store and add fetched data to store.
   */
  _fetchUserData(uid) {
    let fUser;
    let userRef = firebase.database().ref('users/' + uid)
    userRef.on('value', (snapshot) => {
      if (snapshot.val()) {
        fUser = snapshot.val();
        this.props.updateUserTutorial(fUser.hasTutorialMoment);
        this.props.updateEmailUser(fUser.email);
        this.props.updateUsernameUser(fUser.username);
        this.props.updateBirthdayUser(fUser.birthday);
        this.props.updateTypUser(fUser.typ);
        if (fUser.abos) this.props.updateAbosUser(fUser.abos);
        this.props.updateUIDUser(fUser.uid);
        this.props.updateCantonUser(fUser.canton)
        this.props.updateUserPoints(fUser.points);
        this.props.updateUserAwards(fUser.awards);
        if (!fUser.hasTutorialMoment && !tutorialFetched) this._fetchTutorial(fUser.uid)
        this.props.updateUserFeaturesUsed(fUser.featuresUsed);
      }
    })
    registerForPushNotificationsAsync(uid);
    let momentRef = firebase.database().ref('users/' + uid + '/moments').orderByChild('created').limitToFirst(50);
    momentRef.on('value', (snapshot) => {
      if (this.props.user.momentsSynchronised) {
        this.props.updateMomentsUser(snapshot.val());
      }
    })
  }

  /**
     * Get abos from firebase and add fetched data to store.
     */
  _fetchAboData() {
    let fAbo;
    firebase.database().ref('momentAbos').once('value', function (snapshot) {
      fAbo = snapshot.val();
    }).then(() => {
      this.props.updateAbos(fAbo);
    }).catch((error) => {
      console.log(error)
    })
  }

  /**
   * Get news from firebase and add fetched data to store.
   */
  _fetchNewsData() {
    let fNews;
    firebase.database().ref('posts').once('value', function (snapshot) {
      fNews = snapshot.val();
    }).then(() => {
      this.props.updateNews(fNews);
    }).catch((error) => {
      console.log(error)
    })
  }

  /**
     * Get tutorialMoment from firebase and add fetched data to store.
     */
  _fetchTutorial(uid) {
    let fTutorial;
    tutorialFetched = true
    firebase.database().ref('tutorialMoment').once('value', function (snapshot) {
      fTutorial = Object.values(snapshot.val())[0];
    }).then(() => {
      firebase.database().ref('users/' + uid + '/moments').push({
        title: fTutorial.title,
        status: 0,
        steps: fTutorial.steps,
        points: fTutorial.points,
        description: fTutorial.description,
        seen: false,
        created: Date.now()
      }, (error) => {
        if (error) {
          console.log(error)
        } else {
          firebase.database().ref('users/' + uid).update({
            hasTutorialMoment: true
          })
        }
      });
    }).catch((error) => {
      console.log(error)
    })
  }

  render() {
    return (
      <LoadingView/>
    );
  }
}

const mapStatetoProps = state => {
  return {
    user: state.user,
    isConnected: state.network.isConnected
  };
};

export default connect(mapStatetoProps, {
  updateUserToken,
  updateAbosUser,
  updateBirthdayUser,
  updateTypUser,
  updateUsernameUser,
  updateEmailUser,
  updateUIDUser,
  updateMomentsUser,
  updateUserPoints,
  updateCantonUser,
  updateConnectivity,
  updateAbos,
  updateNews,
  updateUserTutorial,
  updateUserAwards,
  updateUserFeaturesUsed
})(AuthLoadingScreen);