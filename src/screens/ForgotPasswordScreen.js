import React from 'react';
import { Content } from 'native-base';
import  ForgotPassword  from '../components/ForgotPassword/ForgotPassword';

/**
 * Screen for forgotten password
 */
export default class ForgotPasswordScreen extends React.Component {
    static navigationOptions = {
        title: 'ForgotPassword',
    };

  render() {
    return (
      <Content>
          {/* Passing navigation to ForgotPassword Component */}
          <ForgotPassword navigation = {this.props.navigation}/>
      </Content>
    );
  }
}