import React from 'react';
import { Image, KeyboardAvoidingView } from 'react-native';
import { Header } from 'react-navigation';
import { Content } from 'native-base';
import Generator from '../components/Generator/Generator'
import AppHeaderIcons from '../components/AppHeaderIcons/AppHeaderIcons'
import stylesHeader from '../components/AppHeaderIcons/stylesHeader';


/**
 * Screen for random generator
 */
export default class GeneratorScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = ({navigation}) => {
        return {
            headerRight: (
                <AppHeaderIcons navigation={navigation}/>
            ),
            headerLeft: (
                <Image style={stylesHeader.logo} source={require('../../assets/images/316_bbs_Logo_rgb.png')}/>
            ),
            }
    };

  render() {
    return (
        <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }} keyboardVerticalOffset = {Header.HEIGHT + 20}>
            <Content>
                <Generator />
            </Content>
        </KeyboardAvoidingView>
    );
  }
}