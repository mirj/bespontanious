import React from 'react';
import { Content } from 'native-base';
import {ImageBackground } from 'react-native'
import Login from '../components/Login/Login';
import stylesLogin from '../components/Login/stylesLogin';

/**
 * Screen for the login
 */
export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = {
        title: 'Login',
    };

    render() {
        return (
            <ImageBackground source={require('../../assets/images/Login.jpg')} resizeMode={'stretch'} imageStyle={stylesLogin.backgroundImage} style={stylesLogin.backgroundImage}>
                <Content>

                    {/* Passing navigation to Login Component */}
                    <Login navigation={this.props.navigation} />

                </Content>
            </ImageBackground>
        );
    }
}