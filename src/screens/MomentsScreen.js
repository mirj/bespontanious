import React from 'react';
import { Image } from 'react-native';
import { Content } from 'native-base';
import stylesCard from '../constants/Styling/stylesCard';
import Moments from '../components/Moments/Moments';
import AppHeaderIcons from '../components/AppHeaderIcons/AppHeaderIcons';
import stylesHeader from '../components/AppHeaderIcons/stylesHeader';

/**
 * Screen for the moments
 */
export default class MomentsScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <AppHeaderIcons navigation={navigation} />
            ),
            headerLeft: (
                <Image style={stylesHeader.logo} source={require('../../assets/images/316_bbs_Logo_rgb.png')}/>
            ),
        }
    };

    render() {
        return (
            <Content style={stylesCard.screenBackground}>
                {/* Passing navigation to news Component */}
                <Moments navigation = {this.props.navigation}/>
            </Content>
        );
    }
}