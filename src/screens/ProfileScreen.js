import React from 'react';
import { Content } from 'native-base';
import User from '../components/User/User';
import AppHeaderIcons from '../components/AppHeaderIcons/AppHeaderIcons';
import AppHeaderIconsLeft from '../components/AppHeaderLeftIcons/AppHeaderLeftIcons';

/**
 * Screen for user profile
 */
export default class ProfileScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <AppHeaderIcons navigation={navigation} />
            ),
            headerLeft: (
                <AppHeaderIconsLeft navigation={navigation}/>
            ),
        }
    };

    render() {
        return (
            <Content>
                {/* Passing navigation to user component */}
                <User navigation={this.props.navigation} />
            </Content>
        );
    }
}