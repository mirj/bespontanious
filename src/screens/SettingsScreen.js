import React from 'react';
import { Image } from 'react-native';
import { Content } from 'native-base';
import Settings from '../components/Settings/Settings';
import AppHeaderIcons from '../components/AppHeaderIcons/AppHeaderIcons';
import stylesHeader from '../components/AppHeaderIcons/stylesHeader';

/**
 * Screen for settings
 */
export default class SettingsScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = ({navigation}) => {
        return {
            headerRight: (
                <AppHeaderIcons navigation={navigation}/>
            ),
            headerLeft: (
                <Image style={stylesHeader.logo} source={require('../../assets/images/316_bbs_Logo_rgb.png')}/>
            ),
            }
    };

    render() {
        return (
            <Content>
                {/* Passing navigation to setting component */}
                <Settings navigation={this.props.navigation} />
            </Content>
        );
    }
}