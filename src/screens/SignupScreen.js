import React from 'react';
import {Content} from 'native-base';
import { ImageBackground } from 'react-native'
import SignUp from '../components/SignUp/SignUp';

/**
 * Screen for signup
 */
export default class SignupScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = {
        title: 'Registrieren',
    };

    render() {
        return (
            <ImageBackground source={require('../../assets/images/Registrieren.jpg')} resizeMode={'stretch'} imageStyle={stylesLogin.backgroundImage} style={stylesLogin.backgroundImage}>
                <Content>
                    {/* Passing navigation to signup component */}
                    <SignUp navigation={this.props.navigation} />
                </Content>
            </ImageBackground>
        );
    }
}