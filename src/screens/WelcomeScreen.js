import React from 'react';
import { Content, Footer, FooterTab, Button, Text, Container, View } from 'native-base';
import { StyleSheet, ImageBackground } from 'react-native';

/**
 * Screens for welcome
 */
export default class WelcomeScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    static navigationOptions = {
        title: 'Welcome',
    };

    render() {
        return (
            <Container>
                <ImageBackground source={require('../../assets/images/WelcomeScreen.jpg')} style={styles.backgroundImage}>
                </ImageBackground>
                <View style={styles.container}>
                    <Text style={styles.textZeile}>RAUS AUS DEM ALLTAG – UNBESCHWERT UND FREI SEIN</Text>
                </View>
                <Content />
                <Footer>
                    <FooterTab style={styles.footerTab}>
                        {/* Buttons navigates to signUp */}
                        <Button style={styles.button} onPress={() => this.props.navigation.navigate('SignUp')}>
                            <Text style={styles.textRegistrieren}>Registrieren</Text>
                        </Button>
                        {/* Buttons navigates to login */}
                        <Button onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={styles.textLogin}>Login</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    textLogin: {
        color: '#707070',
        fontSize: 15,
        position: 'absolute',
        bottom: 15,
    },
    footerTab: {
        backgroundColor: '#EBEBEB'
    },
    button: {
        backgroundColor: '#FCEC45'
    },
    textRegistrieren: {
        color: '#707070',
        fontSize: 15,
        position: 'absolute',
        bottom: 15,
    },
    backgroundImage: {
        flex: -1,
        width: '100%',
        height: '100%',
    },
    textZeile: {
        position: 'absolute',
        bottom: 25,
        fontSize: 25,
        fontStyle: 'normal',
        color: '#fff',
        fontWeight: 'bold',
        padding: 25,
        textAlign: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});